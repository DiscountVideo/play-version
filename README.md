#### Architecture

This project consists of a Play Framework 2-based backend (comprising all the repo's
contents except for those in the angular-app folder) written in Scala, and an Angular 9
frontend (residing in the angular-app folder) written in TypeScript.

The Scala storage sub-project provides models and repositories for the server sub-project.
The repositories interface with the project's MongoDB database (named DiscountVideo) using 
the standard Mongo driver for Scala from scala.mongodb.org.

The frontend is divided into two logical apps, one for customers and one for system 
administrators.  These two apps are actually just modules within a parent Angular
application. That application's main module decides which of the two apps to spin up
depending on the routes to which the user navigates.  The customer app's routes 
all start with "app", while the admin's app routes all start with "admin".  There is 
a fourth module containing code entities and other artifacts which are shared between
the two apps. 
 
#### Running the apps

Start the database with a command like this from the database's root folder:

"C:\Program Files\MongoDB\Server\4.2\bin\mongod.exe" --dbpath .\data

Then, you have to run the server.  The way I do this is to create a Play 2 App 
configuration for it in Intellij IDEA, specifying the "server" module as the one to run.
Enable the auto-reload.  Debugging the server application works as expected using the
default configuration settings.

Note that you must create an email.conf file in the server/conf folder which 
contains host, port, login, and password keys set with to the corresponding 
values for the e-mail account you wish to use for sending notifications to customers 
and admins.  Obviously, the file must never be added to the project's repo.  Here is 
an example of specifying the keys: 

```
host = "foo.hosting.net"
port = 465
login = "foo@bar.com"
password = "..."
```

The frontend (residing in the angular-app folder) can be served by an Angular development
server with the usual "ng serve" command entered from the frontend project's root folder.
In this case, the customer app can be accessed at http://localhost:4200/app, while the
admin app can be accessed at http://localhost:4200/admin. The frontend can also be built
by entering "npm run build" from the root folder, which will copy a production build of
the app to the Play server's public folder (from which its web server serves content) in a
"dist" sub-folder.  The production build of the app can then be accessed at
http://localhost:9000/app for the customer side, and http://localhost:9000/admin for the
admin side.



