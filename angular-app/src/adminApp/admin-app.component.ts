import {Component} from '@angular/core'
import {SessionRestorationService} from "./session-restoration.service"

@Component({
    selector: 'admin-app',
    templateUrl: './admin-app.component.html',
    styleUrls: ['./admin-app.component.css']
})
export class AdminAppComponent {
    constructor(private sessionRestoration: SessionRestorationService) {
        sessionRestoration.restoreSession()
    }
}
