import {NgModule} from "@angular/core"
import {RouterModule} from "@angular/router"
import {AdminAppRoutes} from "adminApp/admin-app.routes"
import {CustomersService} from "shared/customers/customers.service"
import {OrdersService} from "shared/orders/orders.service"
import {VideosService} from "shared/videos/videos.service"
import {AdminAppComponent} from "./admin-app.component"
import {CommonModule} from "@angular/common"
import {LoginComponent} from "./login/login.component"
import {FormsModule} from "@angular/forms"
import {SharedModule} from "shared/shared.module"
import {NavBarComponent} from "./nav-bar.component"
import {VideosComponent} from "./videos/videos.component"
import {onAdminAppInUse} from "shared/Globals"
import {NgxPaginationModule} from "ngx-pagination"
import {EditVideoComponent} from "./videos/edit-video.component"
import {LogOuterService} from "./login/log-outer.service"
import {HttpClientModule} from "@angular/common/http"
import {NgbModule} from "@ng-bootstrap/ng-bootstrap"
import {AdminsService} from "./admins/admins.service"
import {CustomersComponent} from "./customers/customers.component"
import {ConsignmentComponent} from "./customers/consignment.component"
import {EditCustomerComponent} from "./customers/edit-customer.component"
import {OrdersComponent} from "./orders/orders.component"
import {EditOrderComponent} from "./orders/edit-order.component"
import {ChangePasswordComponent} from "./login/change-password.component"
import {AdminsComponent} from "./admins/admins.component"
import {CreateAdminComponent} from "./admins/create-admin.component"
import {LoginService} from "./login/login.service"
import {SessionRestorationService} from "./session-restoration.service"

@NgModule({
    declarations: [
        AdminAppComponent,
        LoginComponent,
        NavBarComponent,
        VideosComponent,
        EditVideoComponent,
        CustomersComponent,
        EditCustomerComponent,
        ConsignmentComponent,
        OrdersComponent,
        EditOrderComponent,
        AdminsComponent,
        CreateAdminComponent,
        ChangePasswordComponent
    ],
    exports: [],
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        NgbModule,
        NgxPaginationModule,
        SharedModule,
        RouterModule.forChild(AdminAppRoutes)
    ],
    providers: [LoginService, LogOuterService, AdminsService, CustomersService,
        OrdersService, VideosService, SessionRestorationService]
})
export class AdminAppModule {
    constructor() {
        onAdminAppInUse()
    }
}
