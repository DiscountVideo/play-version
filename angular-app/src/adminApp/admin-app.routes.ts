import { AdminAppComponent } from "adminApp/admin-app.component";
import { AdminsComponent } from "adminApp/admins/admins.component";
import { CreateAdminComponent } from "adminApp/admins/create-admin.component";
import { ConsignmentComponent } from "adminApp/customers/consignment.component";
import { CustomersComponent } from "adminApp/customers/customers.component";
import { EditCustomerComponent } from "adminApp/customers/edit-customer.component";
import { ChangePasswordComponent } from "adminApp/login/change-password.component";
import { LoginComponent } from "adminApp/login/login.component";
import { EditOrderComponent } from "adminApp/orders/edit-order.component";
import { OrdersComponent } from "adminApp/orders/orders.component";
import { EditVideoComponent } from "adminApp/videos/edit-video.component";
import { VideosComponent } from "adminApp/videos/videos.component";
import { AuthGuard } from "shared/auth.guard";
import {Routes} from "@angular/router";

export const AdminAppRoutes: Routes = [{
    path: "",
    component: AdminAppComponent,
    children: [
        { path: "", component: LoginComponent },
        {
            path: "videos", component: VideosComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "addVideo/:isRental", component: EditVideoComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "editVideo/:videoId", component: EditVideoComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "customers", component: CustomersComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "addCustomer", component: EditCustomerComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "editCustomer/:customerId",
            component: EditCustomerComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "consignment/:customerId",
            component: ConsignmentComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "orders", component: OrdersComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "editOrder/:orderId", component: EditOrderComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "admins", component: AdminsComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "createAdmin", component: CreateAdminComponent,
            canActivate: [AuthGuard],
        },
        {
            path: "changePassword", component: ChangePasswordComponent,
            canActivate: [AuthGuard],
        },
    ],
}];