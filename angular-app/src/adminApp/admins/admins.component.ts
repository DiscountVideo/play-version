import {Component, OnInit} from "@angular/core"
import {ConfirmModalComponent} from "shared/controls/confirm-modal.component"
import * as _ from "lodash"
import {BsModalService} from "ngx-bootstrap/modal"
import {AdminsService} from "./admins.service"
import {Admin} from "../../shared/models/Admin"

@Component({
    selector: "admin-admins",
    templateUrl: "./admins.component.html"
})
export class AdminsComponent implements OnInit {
    admins: Admin[]

    constructor(private adminsService: AdminsService,
        private modalService: BsModalService) {
    }

    ngOnInit() {
        this.fetchAdmins()
    }

    onShouldNotifyChange(admin: Admin) {
        this.adminsService.updateShouldNotifyValue(admin._id,
            admin.shouldNotifyOfOrders).subscribe()
    }

    onRemoveClick(admin: Admin) {
        // if there's only one admin
        if (this.admins.length <= 1) {
            // don't remove it
            const initialState = {
                title: "Not allowed",
                text: "There must be at least one admin."
            }
            this.modalService.show(ConfirmModalComponent, {initialState})
            return
        }

        const initialState = {
            title: "Confirm",
            text: "Are you sure you want to delete this admin?",
            shouldShowCancel: true,
            onChoiceMade: (confirmed: boolean) => {
                if (confirmed) doDeletion()
            }
        }
        this.modalService.show(ConfirmModalComponent, {initialState})

        const doDeletion = () =>
            this.adminsService.deleteAdmin(admin._id)
                .subscribe(ignore => _.remove(this.admins, a => a._id == admin._id))
    }

    private fetchAdmins() {
        this.adminsService.fetchAdmins().subscribe(admins => this.admins = admins)
    }
}



