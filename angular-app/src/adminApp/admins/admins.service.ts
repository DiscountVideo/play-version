import {catchError} from 'rxjs/operators'
import {Injectable} from "@angular/core"
import {Observable} from "rxjs"
import {HttpClient} from "@angular/common/http"
import {AuthService} from "shared/auth.service"
import {ErrorHandler} from "shared/error-handler.service"
import {serverUrl} from "shared/Constants"
import {Admin} from "../../shared/models/Admin"

@Injectable()
export class AdminsService {
    constructor(
        private http: HttpClient, private auth: AuthService, private onError: ErrorHandler) {}

    private readonly options = () => ({headers: this.auth.httpHeaders})

    fetchAdmin(adminId: string): Observable<Admin> {
        const url = `${serverUrl}api/admin/${adminId}`
        return this.http.get<Admin>(url, this.options()).pipe(
            catchError(error => this.onError.alert("Could not fetch admin", error)))
    }

    fetchAdmins(): Observable<Admin[]> {
        const url = `${serverUrl}api/admins`
        return this.http.get<Admin[]>(url, this.options()).pipe(
            catchError(error => this.onError.alert("Could not fetch admins", error)))
    }

    loginAdmin(emailAddress: string, password: string): Observable<LoginAdminResult> {
        const url = `${serverUrl}api/admin/byCredentials/${emailAddress}/${password}`
        return this.http.get<LoginAdminResult>(url).pipe(
            catchError(error => this.onError.alert("Could not log in", error)))
    }

    changeAdminPassword(adminId: string, newPassword: string): Observable<{}> {
        const url = `${serverUrl}api/admin/changePassword/${adminId}/${newPassword}`
        return this.http.put(url, null, {...this.options(), responseType: 'text'}).pipe(
            catchError(error => this.onError.alert("Could not change password", error)))
    }

    createAdmin(emailAddress: string, password: string): Observable<{}> {
        const url = `${serverUrl}api/admin/${emailAddress}/${password}`
        return this.http.post(url, null, {...this.options(), responseType: 'text'}).pipe(
            catchError(error => this.onError.alert("Could not create admin", error)))
    }

    deleteAdmin(id: string): Observable<{}> {
        const url = `${serverUrl}api/admin/${id}`
        return this.http
            .delete(url, {...this.options(), responseType: 'text'}).pipe(
                catchError(error => this.onError.alert("Could not delete admin", error)))
    }

    updateShouldNotifyValue(adminId: string, value: boolean): Observable<{}> {
        const url = `${serverUrl}api/admin/shouldNotify/${adminId}/${value}`
        return this.http.put(url, null, {...this.options(), responseType: 'text'}).pipe(
            catchError(error =>
                this.onError.alert("Could not update admin should-notify status", error)))
    }
}

export interface LoginAdminResult {
    admin: Admin;
    token: string;
    errorMessage: string;
}

