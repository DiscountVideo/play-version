import {Component} from "@angular/core"
import {isEmailAddressValid} from "../../shared/Validators"
import {Router} from "@angular/router"
import {AdminsService} from "./admins.service"

@Component({
    selector: 'admin-create-admin',
    templateUrl: './create-admin.component.html'
})
export class CreateAdminComponent {
    emailAddress: string
    password: string
    isAddressInUse = false

    constructor(private adminsService: AdminsService, private router: Router) {}

    shouldDisableCreateButton(): boolean {
        return !isEmailAddressValid(this.emailAddress)
            || !this.password || this.password.length < 5
    }

    onCreateClick = () => {
        this.isAddressInUse = false
        this.adminsService.createAdmin(this.emailAddress, this.password)
            .subscribe(
                ignore => this.router.navigateByUrl('/admin/admins'),
                error => {
                    if (error.error && error.error.indexOf("in use") > 0)
                        this.isAddressInUse = true
                }
            )
    }
}

