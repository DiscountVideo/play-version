import {Component, OnInit} from "@angular/core"
import {Customer} from "../../shared/models/Customer"
import {CustomersService} from "../../shared/customers/customers.service"
import {ActivatedRoute} from "@angular/router"

@Component({
    selector: 'admin-consignment',
    templateUrl: './consignment.component.html'
})
export class ConsignmentComponent implements OnInit {
    customer: Customer

    constructor(
        private route: ActivatedRoute, private customersService: CustomersService) {}

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.customersService.fetchCustomer(params['customerId'])
                .subscribe(customer => this.customer = customer)
        })
    }
}

