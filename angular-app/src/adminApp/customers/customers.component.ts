import {Component, OnInit} from "@angular/core"
import {CustomersService} from "shared/customers/customers.service"
import {Router} from "@angular/router"
import {Customer} from "shared/models/Customer"
import {noFirstLetterFilterValue} from "shared/Constants"

@Component({
    selector: "admin-customers",
    templateUrl: "./customers.component.html",
    providers: [CustomersService]
})
export class CustomersComponent implements OnInit {
    customers: Customer[]
    totalCount = 0
    customersPerPage = searchSettings.customersPerPage
    lastNameSearchText = searchSettings.lastNameSearchText
    firstLetter = searchSettings.firstLetter
    pageNumber = searchSettings.pageNumber

    constructor(private customersService: CustomersService,
        private router: Router) {}

    ngOnInit() {
        this.fetchCustomersPage()
    }

    onCustomersPerPageSelect = (numCustomers: number) => {
        this.customersPerPage = searchSettings.customersPerPage = numCustomers
        this.pageNumber = 0
        this.fetchCustomersPage()
    }

    onFirstLetterSelect = (letter: string) => {
        // clear the last name search text, as well, since the two might clash
        this.firstLetter = searchSettings.firstLetter = letter
        this.lastNameSearchText = searchSettings.lastNameSearchText = null
        this.pageNumber = 0
        this.fetchCustomersPage()
    }

    onLastNameSearchTextChange = (value: string) => {
        this.lastNameSearchText = searchSettings.lastNameSearchText = value
        this.pageNumber = 0
        this.fetchCustomersPage()
    }

    onPageChange = (number: number) => {
        this.pageNumber = searchSettings.pageNumber = number - 1
        this.fetchCustomersPage()
    }

    onCustomerClick = (customer: Customer) => {
        this.router.navigate(["/admin/editCustomer", customer._id]).then()
    }

    onConsignmentClick = (customer: Customer) => {
        this.router.navigate(["/admin/consignment", customer._id]).then()
    }

    private fetchCustomersPage() {
        this.customersService
            .fetchCustomersPage(
                this.pageNumber, this.firstLetter, this.customersPerPage,
                this.lastNameSearchText)
            .subscribe(reply => {
                this.customers = reply.customers
                this.totalCount = reply.totalCount
            })
    }
}

interface SearchSettings {
    customersPerPage: number;
    lastNameSearchText?: string;
    firstLetter: string;
    pageNumber: number;
}

// stores the settings which should persist across visits to this page
const searchSettings: SearchSettings = {
    customersPerPage: 10,
    firstLetter: noFirstLetterFilterValue,
    pageNumber: 0
}

