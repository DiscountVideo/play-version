import {Component, OnInit} from "@angular/core"
import {Customer} from "../../shared/models/Customer"
import {CustomersService} from "../../shared/customers/customers.service"
import {ActivatedRoute, Router} from "@angular/router"
import {
    isEmailAddressValid,
    isPhoneNumberValid,
    isZipCodeValid
} from "../../shared/Validators"

@Component({
    selector: 'admin-edit-customer',
    templateUrl: './edit-customer.component.html'
})
export class EditCustomerComponent implements OnInit {
    customer: Customer
    isForAdding: boolean

    constructor(
        private route: ActivatedRoute, private router: Router,
        private customersService: CustomersService) {}

    ngOnInit() {
        this.route.url.subscribe(segments => {
            this.isForAdding = segments[segments.length - 1].path == "addCustomer"

            if (this.isForAdding) {
                this.customer = new Customer()
                this.customer._id = null
            } else this.route.params.subscribe(params => {
                this.customersService.fetchCustomer(params['customerId'])
                    .subscribe(customer => this.customer = customer)
            })
        })
    }

    shouldDisableSaveButton() {
        return !this.areCustomerDataValid()
    }

    saveCustomer() {
        if (this.isForAdding) this.addCustomer(); else this.updateCustomer()
    }

    private areCustomerDataValid() {
        // we aren't as stringent about all fields being filled on the admin side,
        // as we are on the customer side
        const c = this.customer
        return c.firstName && c.firstName.length > 0
            && c.lastName && c.lastName.length > 0
            && (!c.zipCode || isZipCodeValid(c.zipCode))
            && (!c.phoneNumber || isPhoneNumberValid(c.phoneNumber))
            && (!c.emailAddress || isEmailAddressValid(c.emailAddress))
    }

    private goToCustomersPage = ignore => this.router.navigateByUrl('/admin/customers')

    private addCustomer() {
        this.customersService.addCustomer(this.customer)
            .subscribe(this.goToCustomersPage)
    }

    private updateCustomer() {
        this.customersService.updateCustomer(this.customer)
            .subscribe(this.goToCustomersPage)
    }
}
