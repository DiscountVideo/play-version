import {Component} from "@angular/core"
import {AdminsService} from "../admins/admins.service"
import {LoginService} from "./login.service"

@Component({
    selector: 'admin-change-password',
    templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent {
    constructor(private adminsService: AdminsService,
        private loginService: LoginService,
    ) {}

    makeChange = (newPassword: string) => {
        return this.adminsService.changeAdminPassword(
            this.loginService.loggedInAdmin._id, newPassword)
    }
}