import {Injectable} from "@angular/core"
import {AuthService} from "shared/auth.service"
import {Router} from "@angular/router"

@Injectable()
export class LogOuterService {
    private loginStillValidIntervalId = -1

    constructor(private auth: AuthService, private router: Router) {}

    logOut() {
        // stop checking at intervals to determine if the token is still valid
        if (this.loginStillValidIntervalId >= 0)
            window.clearInterval(this.loginStillValidIntervalId)

        this.auth.logOut()
        this.router.navigateByUrl("/admin").then()
    }

    startTokenExpirationChecks() {
        // institute a check at intervals whether the token is still valid,
        // logging the user out when it isn't
        this.loginStillValidIntervalId =
            window.setInterval(
                () => {if (!this.auth.isLoggedIn()) this.logOut()},
                10 * 1000
            )
    }
}