import {map} from 'rxjs/operators'
import {Component} from '@angular/core'
import {AuthService} from "../../shared/auth.service"
import {Router} from "@angular/router"
import {LogOuterService} from "./log-outer.service"
import {Observable} from "rxjs"
import {AdminsService} from "../admins/admins.service"
import {LoginService} from "./login.service"

@Component({
    selector: 'admin-login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    constructor(
        private auth: AuthService, private router: Router,
        private loginService: LoginService,
        private logOuter: LogOuterService,
        private adminsService: AdminsService) {}

    logIn = (emailAddress: string, password: string): Observable<boolean> => {
        return this.adminsService.loginAdmin(emailAddress, password).pipe(
            map(result => {
                if (result.token) this.loginService.handleLogin(result.admin,
                    result.token)
                return !result.errorMessage
            }))
    }
}
