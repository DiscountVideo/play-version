import {Injectable} from "@angular/core"
import {Admin} from "../../shared/models/Admin"
import {keys} from "../../shared/Constants"
import {AuthService} from "../../shared/auth.service"
import {Router} from "@angular/router"
import {LogOuterService} from "./log-outer.service"

@Injectable()
export class LoginService {
    loggedInAdmin: Admin

    constructor(
        private auth: AuthService, private router: Router,
        private logOuter: LogOuterService) {}

    handleLogin(admin: Admin, token: string) {
        this.loggedInAdmin = admin

        // save the JWT supplied by the server, to be included in
        // all future web API calls
        this.auth.saveToken(token)

        this.logOuter.startTokenExpirationChecks()

        // store in local storage the ID of the logged-in admin, so this
        // admin-app can restore its knowledge of who is using it after any page refresh
        window.localStorage[keys.adminId] = admin._id

        // start the logged-in admin on the admin-videos page
        this.router.navigateByUrl(`/admin/videos`).then()
    }
}