import {Component} from '@angular/core'
import {Router} from "@angular/router"
import {AuthService} from "../shared/auth.service"
import {LogOuterService} from "./login/log-outer.service"

@Component({
    selector: 'admin-nav-bar',
    templateUrl: './nav-bar.component.html'
})
export class NavBarComponent {
    constructor(public router: Router, public auth: AuthService,
        public logOuter: LogOuterService) {}
}

