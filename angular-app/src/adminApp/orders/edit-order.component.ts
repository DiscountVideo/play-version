import {Component, OnInit} from "@angular/core"
import {ActivatedRoute} from "@angular/router"
import * as _ from "lodash"
import {Order, OrderVideo} from "shared/models/Order"
import {OrdersService} from "shared/orders/orders.service"

@Component({
    selector: "admin-edit-order",
    templateUrl: "./edit-order.component.html"
})
export class EditOrderComponent implements OnInit {
    order: Order

    constructor(
        private ordersService: OrdersService,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params.subscribe(params =>
            this.fetchOrder(params["orderId"])
        )
    }

    get sortedVideos() {
        return _.sortBy(this.order.videos, v => v.title)
    }

    doesOrderContainRentals(): boolean {
        return this.order.rentalsDueDate != null
    }

    isReturned(): boolean {
        return !_.find(this.order.videos, v => !v.isReturned)
    }

    isOverdue(): boolean {
        return !this.isReturned()
            && new Date().getTime() > this.order.rentalsDueDate.getTime()
    }

    getDaysOverdue(): number {
        const msInDay = 1000 * 3600 * 24
        return Math.floor(
            (new Date().getTime() - this.order.rentalsDueDate.getTime()) /
            msInDay)
    }

    updateIsShipped() {
        this.ordersService.updateIsShipped(this.order)
            // when an error occurs, undo the change in the view,
            // as it could not be persisted
            .subscribe(
                {error: () => this.order.isShipped = !this.order.isShipped})
    }

    updateIsReturned(video: OrderVideo) {
        this.ordersService.updateIsReturned(this.order._id, video)
            // when an error occurs, undo the change in the view,
            // as it could not be persisted
            .subscribe({error: () => video.isReturned = !video.isReturned})
    }

    onMarkAllAsReturnedClick() {
        // for each rental video in the order
        this.order.videos.forEach(video => {
            // if this video is already marked as returned, skip it
            if (video.isReturned) return

            // mark this video as returned, and get the change stored
            video.isReturned = true
            this.ordersService.updateIsReturned(this.order._id, video)
                // when an error occurs, undo the change in the view,
                // as it could not be persisted
                .subscribe({error: () => video.isReturned = !video.isReturned})
        })
    }

    private fetchOrder(id: string) {
        this.ordersService.fetchOrder(id).subscribe(order => this.order = order)
    }
}
