import {Component, OnInit} from "@angular/core"
import {Router} from "@angular/router"
import {noFirstLetterFilterValue} from "shared/Constants"
import {OrdersService} from "shared/orders/orders.service"
import {Order} from "shared/models/Order"
import * as _ from "lodash"

@Component({
    selector: "admin-orders",
    templateUrl: "./orders.component.html",
    providers: [OrdersService]
})
export class OrdersComponent implements OnInit {
    orders: Order[]
    totalCount = 0
    ordersPerPage = searchSettings.ordersPerPage
    customerNameSearchText = searchSettings.customerNameSearchText
    firstLetter = searchSettings.firstLetter
    pageNumber = searchSettings.pageNumber
    shouldExcludeShipped = searchSettings.shouldExcludeShipped
    shouldLimitToOverdue = searchSettings.shouldLimitToOverdue
    shouldExcludeReturned = searchSettings.shouldExcludeReturned

    constructor(private ordersService: OrdersService, private router: Router) {}

    ngOnInit() {
        this.fetchOrdersPage()
    }

    onOrdersPerPageSelect = (numOrders: number) => {
        this.ordersPerPage = searchSettings.ordersPerPage = numOrders
        this.pageNumber = 0
        this.fetchOrdersPage()
    }

    onFirstLetterSelect = (letter: string) => {
        // clear the customer name search text, as well, since the two might clash
        this.firstLetter = searchSettings.firstLetter = letter
        this.customerNameSearchText =
            searchSettings.customerNameSearchText = null
        this.pageNumber = 0
        this.fetchOrdersPage()
    }

    onCustomerNameSearchTextChange = (value: string) => {
        this.customerNameSearchText =
            searchSettings.customerNameSearchText = value
        this.pageNumber = 0
        this.fetchOrdersPage()
    }

    onShouldExcludeShippedChange = () => {
        searchSettings.shouldExcludeShipped = this.shouldExcludeShipped
        this.pageNumber = 0
        this.fetchOrdersPage()
    }

    onShouldLimitToOverdueChange = () => {
        searchSettings.shouldLimitToOverdue = this.shouldLimitToOverdue
        this.pageNumber = 0
        this.fetchOrdersPage()
    }

    onShouldExcludeReturnedChange = () => {
        searchSettings.shouldExcludeReturned = this.shouldExcludeReturned
        this.pageNumber = 0
        this.fetchOrdersPage()
    }

    onPageChange = (number: number) => {
        this.pageNumber = searchSettings.pageNumber = number - 1
        this.fetchOrdersPage()
    }

    onOrderClick = (order: Order) => {
        this.router.navigate(["/admin/editOrder", order._id]).then()
    }

    isOverdue(order: Order): boolean {
        return order.rentalsDueDate
            && new Date().getTime() > order.rentalsDueDate.getTime()
    }

    isNotYetReturned(order: Order): boolean {
        return !!_.find(order.videos, v => v.isRental && !v.isReturned)
    }

    private fetchOrdersPage() {
        // if customer name search text is specified, or no first letter is specified,
        // don't use a first letter filter
        const shouldUseFirstLetter =
            !this.customerNameSearchText && this.firstLetter !=
            noFirstLetterFilterValue

        this.ordersService
            .fetchOrdersPage(
                this.pageNumber,
                shouldUseFirstLetter ? this.firstLetter :
                    noFirstLetterFilterValue,
                this.ordersPerPage,
                this.customerNameSearchText, this.shouldExcludeShipped,
                this.shouldLimitToOverdue, this.shouldExcludeReturned)
            .subscribe(reply => {
                this.orders = reply.orders
                this.totalCount = reply.totalCount
            })
    }
}

interface SearchSettings {
    ordersPerPage: number;
    customerNameSearchText?: string;
    firstLetter: string;
    pageNumber: number;
    shouldExcludeShipped: boolean;
    shouldLimitToOverdue: boolean;
    shouldExcludeReturned: boolean;
}

// stores the settings which should persist across visits to this page
const searchSettings: SearchSettings = {
    ordersPerPage: 10,
    firstLetter: noFirstLetterFilterValue,
    pageNumber: 0,
    shouldExcludeShipped: false,
    shouldLimitToOverdue: false,
    shouldExcludeReturned: false
}

