import {Injectable} from "@angular/core"
import {AuthService} from "../shared/auth.service"
import {ErrorHandler} from "../shared/error-handler.service"
import {LogOuterService} from "./login/log-outer.service"
import {Store} from "@ngrx/store"
import {keys} from "../shared/Constants"
import {AdminsService} from "./admins/admins.service"
import {LoginService} from "./login/login.service"

@Injectable()
export class SessionRestorationService {
    constructor(
        private auth: AuthService,
        private adminsService: AdminsService,
        private loginService: LoginService,
        private onError: ErrorHandler,
        private logOuter: LogOuterService) {}

    restoreSession(): void {
        // if there is still an admin logged in from the previous session,
        // try to reload it
        if (this.auth.isLoggedIn()) this.fetchAdmin()
    }

    /**
     * Tries to fetch from the server the admin whose ID has been stored
     * in local storage as the last one to be logged into the app from
     * the user's machine.  If the admin is fetched, it is considered to
     * still be logged in.
     */
    private fetchAdmin() {
        // if we can reload the customer's info from the server
        const id = window.localStorage.getItem(keys.adminId)
        this.adminsService.fetchAdmin(id)
            .subscribe(
                admin => {
                    // skip the login page, as the admin is now logged in again
                    this.loginService.loggedInAdmin = admin
                    this.logOuter.startTokenExpirationChecks()
                },
                error => {
                    this.onError.alert("Could not reload admin info", error)
                    this.logOuter.logOut()
                }
            )
    }
}