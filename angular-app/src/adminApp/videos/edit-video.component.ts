import {Component, OnInit} from "@angular/core"
import {ConfirmModalComponent} from "shared/controls/confirm-modal.component"
import {Video} from "shared/models/Video"
import {VideosService} from "shared/videos/videos.service"
import {ActivatedRoute, Router} from "@angular/router"
import {CustomerName, CustomersService} from "shared/customers/customers.service"
import {BsModalService} from "ngx-bootstrap/modal"

@Component({
    selector: "admin-edit-video",
    templateUrl: "./edit-video.component.html"
})
export class EditVideoComponent implements OnInit {
    video: Video
    isForAdding: boolean
    tag: string
    tags: string[]
    studios: string[]
    customerNames: CustomerName[]

    constructor(
        private videosService: VideosService,
        private customersService: CustomersService,
        private route: ActivatedRoute,
        private router: Router, private modalService: BsModalService) {
    }

    ngOnInit() {
        this.route.url.subscribe(segments => {
            this.route.params.subscribe(params => {
                this.isForAdding =
                    segments[segments.length - 2].path == "addVideo"

                if (this.isForAdding) {
                    const video = {
                        isRental: params["isRental"] == "true",
                        _id: null
                    } as Video
                    this.onVideoFetched(video)
                } else this.fetchVideo(params["videoId"])

                this.fetchCustomerNames()
            })
        })
    }

    isYearValid() {
        const year = this.video.year
        return !year || (year >= 1900 && year <= 2050)
    }

    onConsignmentCustomerSelected() {
        if (!this.video.consignmentCustomerId) return

        // also assign today's date as the video's consignment (beginning) date
        this.video.consignmentDate = new Date()
    }

    shouldDisableSaveButton() {
        const title = this.video.title
        return !title || title.trim().length == 0
            || (this.video.year && !this.isYearValid())
    }

    saveVideo() {
        if (this.isForAdding) this.addVideo(); else this.updateVideo()
    }

    deleteVideo() {
        const initialState = {
            title: "Confirm",
            text: "Are you sure you want to delete this video?",
            shouldShowCancel: true,
            onChoiceMade: (confirmed: boolean) => {
                if (confirmed) doDeletion()
            }
        }
        this.modalService.show(ConfirmModalComponent, {initialState})

        const doDeletion = () =>
            this.videosService.deleteVideo(this.video._id)
                .subscribe(this.goToVideosPage)
    }

    private fetchVideo(id: string) {
        this.videosService.fetchVideo(id)
            .subscribe(video => this.onVideoFetched(video))
    }

    private onVideoFetched(video: Video) {
        this.video = video
        this.fetchTags()
        this.fetchStudios()
    }

    private fetchTags() {
        const fetch =
            this.isForAdding
                ? this.videosService.fetchAllTags()
                : this.videosService.fetchTags(this.video.isRental)
        fetch.subscribe(tags => this.tags = tags)
    }

    private fetchStudios() {
        const fetch =
            this.isForAdding
                ? this.videosService.fetchAllStudios()
                : this.videosService.fetchStudios(this.video.isRental)
        fetch.subscribe(studios => this.studios = studios)
    }

    private fetchCustomerNames() {
        this.customersService.fetchCustomerNames()
            .subscribe(names => this.customerNames = names)
    }

    private goToVideosPage = ignore => this.router.navigateByUrl(
        "/admin/videos")

    private addVideo() {
        this.videosService.addVideo(this.video)
            .subscribe(this.goToVideosPage)
    }

    private updateVideo() {
        this.videosService.updateVideo(this.video)
            .subscribe(this.goToVideosPage)
    }
}
