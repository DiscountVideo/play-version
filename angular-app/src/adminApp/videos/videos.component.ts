import {Component, OnInit} from "@angular/core"
import {Video} from "shared/models/Video"
import {noFirstLetterFilterValue} from "shared/Constants"
import {videoTypes} from "shared/videos/VideoTypes"
import {VideosService} from "shared/videos/videos.service"
import {Router} from "@angular/router"

@Component({
    selector: "admin-videos",
    templateUrl: "./videos.component.html",
    styleUrls: ['../../shared/controls/search-bar.component.scss']
})
export class VideosComponent implements OnInit {
    videos: Video[]
    totalCount: number
    videoType = searchSettings.videoType
    pageNumber = searchSettings.pageNumber
    videosPerPage = searchSettings.videosPerPage
    firstLetter = searchSettings.firstLetter
    titleSearchText = searchSettings.titleSearchText
    codeSearchText = searchSettings.codeSearchText

    videoTypes = videoTypes

    isForRentals = () => searchSettings.videoType == videoTypes.rentals

    constructor(private videosService: VideosService, private router: Router) {}

    ngOnInit() {
        this.fetchVideosPage()
    }

    onVideoTypeClick(videoType: string) {
        // if the selected videos type is already being viewed, do nothing
        if (videoType == this.videoType) return

        // set the given type as the current type of videos being viewed
        this.videoType = searchSettings.videoType = videoType
        this.pageNumber = searchSettings.pageNumber = 0
        this.fetchVideosPage()
    }

    onPageChange = (number: number) => {
        this.pageNumber = searchSettings.pageNumber = number - 1
        this.fetchVideosPage()
    }

    onFirstLetterSelect = (letter: string) => {
        // clear the title search text, as well, since the two might clash
        this.firstLetter = searchSettings.firstLetter = letter
        this.titleSearchText = searchSettings.titleSearchText = null
        this.pageNumber = 0
        this.fetchVideosPage()
    }

    onVideosPerPageSelect = (numVideos: number) => {
        this.videosPerPage = searchSettings.videosPerPage = numVideos
        this.pageNumber = 0
        this.fetchVideosPage()
    }

    onTitleSearchTextChange = (value: string) => {
        this.titleSearchText = searchSettings.titleSearchText = value
        this.pageNumber = 0
        this.fetchVideosPage()
    }

    onCodeSearchTextChange = (value: string) => {
        this.codeSearchText = searchSettings.codeSearchText = value
        this.pageNumber = 0
        this.fetchVideosPage()
    }

    onVideoClick = (video: Video) => {
        this.router.navigate(["/admin/editVideo", video._id]).then()
    }

    private fetchVideosPage() {
        this.videosService
            .fetchVideosPage(
                this.isForRentals(),
                this.pageNumber, this.firstLetter, this.videosPerPage,
                this.titleSearchText, null, null,
                this.isForRentals() ? this.codeSearchText : null)
            .subscribe(reply => {
                this.videos = reply.videos
                this.totalCount = reply.totalCount
            })
    }
}

interface SearchSettings {
    videoType: string;
    videosPerPage: number;
    titleSearchText?: string;
    codeSearchText?: string;
    firstLetter: string;
    pageNumber: number;
}

// stores the settings which should persist across visits to this page
const searchSettings: SearchSettings = {
    videoType: videoTypes.rentals,
    videosPerPage: 10,
    firstLetter: noFirstLetterFilterValue,
    pageNumber: 0
}

