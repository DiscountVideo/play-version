import {Component} from '@angular/core'

@Component({
    selector: 'dv-app',
    template: '<router-outlet></router-outlet>'
})
export class AppComponent {
}
