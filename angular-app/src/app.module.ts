import {HttpClientModule} from "@angular/common/http"
import {NgModule} from "@angular/core"
import {BrowserModule} from "@angular/platform-browser"
import {RouterModule} from "@angular/router"
import {AppComponent} from "app.component"
import {AuthService} from "shared/auth.service"
import {AuthGuard} from "shared/auth.guard"
import {ErrorHandler} from "shared/error-handler.service"

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        HttpClientModule,
        RouterModule.forRoot([
    { path: "", pathMatch: "full", redirectTo: "app" },
    {
        path: "admin",
        loadChildren: () => import("adminApp/admin-app.module").then(m => m.AdminAppModule)
    },
    {
        path: "app",
        loadChildren: () => import("customerApp/customer-app.module").then(m => m.CustomerAppModule)
    }
], { relativeLinkResolution: 'legacy' })
    ],
    providers: [AuthService, AuthGuard, ErrorHandler],
    entryComponents: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}

