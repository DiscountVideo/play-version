export const ageWarning1 =
    "This site contains adult material of an extreme nature.  You must be at least" +
    " 18 years of age, depending on the age of majority in your jurisdiction," +
    " to access DI$COUNT VIDEO.  If it is illegal to view adult material" +
    " in your community, please leave now."

export const ageWarning2 =
    "We cannot be held accountable for your actions. We are not acting in any" +
    " way to send you this information you are choosing to receive it." +
    " Continuing further means that you understand and accept responsibility" +
    " for your own actions, thus releasing the creators of this web page" +
    " and our service provider for all liability."

