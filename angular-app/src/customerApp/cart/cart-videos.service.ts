import {catchError} from 'rxjs/operators'
import {HttpClient} from "@angular/common/http"
import {AuthService} from "shared/auth.service"
import {ErrorHandler} from "shared/error-handler.service"
import {Injectable} from "@angular/core"
import {Video} from "shared/models/Video"
import {Observable} from "rxjs"
import {serverUrl} from "shared/Constants"

@Injectable()
export class CartVideosService {
    constructor(
        private http: HttpClient, private auth: AuthService,
        private onError: ErrorHandler) {}

    private readonly options = () => ({headers: this.auth.httpHeaders})

    fetchVideos(customerId: string): Observable<FetchCartVideosResult> {
        const url = `${serverUrl}api/cartVideos/${customerId}`
        return this.http.get<FetchCartVideosResult>(url, this.options()).pipe(
            catchError(error => this.onError.alert("Could not fetch cart videos", error)))
    }

    addVideo(videoId: string, customerId: string): Observable<{}> {
        const url = `${serverUrl}api/cartVideos/${videoId}/${customerId}`
        return this.http.post(url, null, {...this.options(), responseType: 'text'}).pipe(
            catchError(error => this.onError.alert("Could not add video to cart", error)))
    }

    removeVideo(videoId: string): Observable<{}> {
        const url = `${serverUrl}api/cartVideos/${videoId}`
        return this.http.delete(url, {...this.options(), responseType: 'text'}).pipe(
            catchError(
                error => this.onError.alert("Could not remove video from cart", error)))
    }
}

export interface FetchCartVideosResult {
    rentalVideos: Video[];
    forSaleVideos: Video[];
}

