import { createAction, props } from "@ngrx/store"
import { Video } from "shared/models/Video"

export const CartActions = {
    videoAddition: createAction("[Cart] Video addition",
        props<{ video: Video }>()),
    videoRemoval: createAction("[Cart] Video removal",
        props<{ video: Video }>()),
    orderCompletion: createAction("[Cart] Order completion"),
    refresh: createAction("[Cart] Refresh",
        props<{ rentalVideos: Video[], forSaleVideos: Video[] }>()),
}


