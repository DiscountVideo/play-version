import {Component} from "@angular/core"
import {Store} from "@ngrx/store"
import {BsModalService} from "ngx-bootstrap/modal"
import {CartService} from "customerApp/cart/cart.service"
import {Video} from "shared/models/Video"
import {VideoComponent} from "../videos/video.component"
import {prices} from "shared/Constants"
import {Customer} from "../../shared/models/Customer"
import {selectCustomer, selectCustomerIsRenting} from "../customer/customer.reducer"
import {selectCartForSaleVideos, selectCartRentalVideos} from "./cart.reducer"

@Component({
    selector: "customer-cart",
    templateUrl: "./cart.component.html"
})
export class CartComponent {
    customer: Customer

    isRenting: boolean

    prices = prices

    rentalVideos: Video[]

    forSaleVideos: Video[]

    constructor(public cart: CartService,
        private modalService: BsModalService, private store: Store) {
        store.select(selectCustomer).subscribe(customer => this.customer = customer)
        store.select(selectCustomerIsRenting).subscribe(value => this.isRenting = value)
        store.select(selectCartRentalVideos)
            .subscribe(videos => this.rentalVideos = videos)
        store.select(selectCartForSaleVideos)
            .subscribe(videos => this.forSaleVideos = videos)
    }

    onVideoClick = (video: Video) => {
        this.modalService.show(VideoComponent, {initialState: {video}})
    }
}
