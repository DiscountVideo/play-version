import {Action, createReducer, createSelector, on} from "@ngrx/store"
import {Video} from "shared/models/Video"
import {CartActions} from "customerApp/cart/cart.actions"
import {selectCartState} from "../customer-app.state"

export interface CartState {
    rentalVideos: Video[];
    forSaleVideos: Video[];
}

export const initialCartState: CartState = {
    rentalVideos: [],
    forSaleVideos: []
}

export const selectCartRentalVideos = createSelector(selectCartState,
    (state: CartState) => state.rentalVideos)
export const selectCartForSaleVideos = createSelector(selectCartState,
    (state: CartState) => state.forSaleVideos)

const CartReducer = createReducer(initialCartState,
    on(CartActions.videoAddition,
        (state, {video}) => addVideo(state, video)
    ),
    on(CartActions.videoRemoval,
        (state, {video}) => removeVideo(state, video)
    ),
    on(CartActions.orderCompletion,
        (state) => ({
            ...state,
            rentalVideos: [],
            forSaleVideos: []
        })
    ),
    on(CartActions.refresh,
        (state, {rentalVideos, forSaleVideos}) => ({
            ...state,
            rentalVideos,
            forSaleVideos
        })
    )
)

export function cartReducer(state: CartState | undefined, action: Action) {
    return CartReducer(state, action)
}

function addVideo(state_: CartState, video: Video): CartState {
    const state = {...state_}
    const videos_ = video.isRental ? state.rentalVideos : state.forSaleVideos
    const videos = [...videos_, video]
    if (video.isRental) state.rentalVideos = videos
    else state.forSaleVideos = videos
    return state
}

function removeVideo(_state: CartState, video: Video): CartState {
    const state = {..._state}
    const videos_ = video.isRental ? state.rentalVideos : state.forSaleVideos
    const videos = videos_.filter(v => v != video)
    if (video.isRental) state.rentalVideos = videos
    else state.forSaleVideos = videos
    return state
}