import {Store} from "@ngrx/store"
import {Customer} from "shared/models/Customer"
import {CartActions} from "customerApp/cart/cart.actions"
import * as _ from "lodash"
import {Video} from "shared/models/Video"
import {Injectable} from "@angular/core"
import {videoStatuses} from "shared/videos/VideoStatuses"
import {prices} from "shared/Constants"
import {CurrencyPipe} from "@angular/common"
import {Observable} from "rxjs/internal/Observable"
import {tap} from "rxjs/operators"
import {CartVideosService, FetchCartVideosResult} from "./cart-videos.service"
import {selectCustomer, selectCustomerIsRenting} from "../customer/customer.reducer"
import {selectCartForSaleVideos, selectCartRentalVideos} from "./cart.reducer"

@Injectable()
export class CartService {
    private customer: Customer

    private isRenting: boolean

    private rentalVideos: Video[]

    private forSaleVideos: Video[]

    constructor(
        private cartVideosService: CartVideosService,
        private store: Store,
        private currencyPipe: CurrencyPipe) {
        store.select(selectCustomer).subscribe(customer => this.customer = customer)
        store.select(selectCustomerIsRenting).subscribe(value => this.isRenting = value)
        store.select(selectCartRentalVideos)
            .subscribe(videos => this.rentalVideos = videos)
        store.select(selectCartForSaleVideos)
            .subscribe(videos => this.forSaleVideos = videos)
    }

    /**
     * Returns whether this cart is empty of items.
     */
    isEmpty(): Boolean {
        return this.rentalVideos.length == 0 && this.forSaleVideos.length == 0
    }

    /**
     * Returns a list of all videos in this cart.
     */
    getAllVideos(): Video[] {
        return _.concat(this.rentalVideos, this.forSaleVideos)
    }

    /**
     * Returns whether this cart contains the given video.
     */
    containsVideo(video: Video): boolean {
        const videos = video.isRental ? this.rentalVideos : this.forSaleVideos
        return videos.some(v => v._id == video._id)
    }

    refresh(): Observable<{}> {
        return this.cartVideosService.fetchVideos(this.customer._id)
            .pipe(tap((result: FetchCartVideosResult) => this.store.dispatch(
                CartActions.refresh(result))))
    }

    addVideo(video: Video) {
        // when we've added the given video to the cart in the database
        this.cartVideosService.addVideo(video._id, this.customer._id)
            .subscribe(ignore => {
                // mark the video as being in the customer's cart
                video.status = videoStatuses.inCart.id
                video.statusCustomerId = this.customer._id

                // add a copy of the video to the cart in the local store
                this.store.dispatch(CartActions.videoAddition(
                    {video: {...video}}))
            })
    }

    removeVideo(video: Video) {
        // when we've removed the given video from the cart in the database
        this.cartVideosService.removeVideo(video._id)
            .subscribe(ignore => {
                // remove the video from the cart in the local store
                this.store.dispatch(CartActions.videoRemoval({video}))
            })
    }

    /**
     * Returns the total price of the items in this cart.
     */
    getTotalPrice() {
        const sum = (a: number, b: Video) => a +
            (b.isRental ? prices.rental : prices.forSale)
        const rentalTotal = this.rentalVideos.reduce(sum, 0)
        const salesTotal = this.forSaleVideos.reduce(sum, 0)
        return rentalTotal + salesTotal
    }

    /**
     * Returns the amount of credit of this cart's customer to apply
     * against the total price of this cart's items.  This amount equals
     * the minimum between the customer's credit amount and the total price.
     */
    getCreditToApply() {
        return this.customer
            ? Math.min(this.customer.credit || 0, this.getTotalPrice())
            : 0
    }

    /**
     * Returns a short summary string of this cart's contents.
     */
    getSummary() {
        // check for an empty cart
        if (this.isEmpty()) return "empty"

        // add the rental count portion
        const rentalCount = this.rentalVideos.length
        let summary = ""
        if (rentalCount > 0)
            summary = rentalCount + " rental" + (rentalCount > 1 ? "s" : "")

        // add the sale count portion
        const saleCount = this.forSaleVideos.length
        if (saleCount > 0)
            summary +=
                (rentalCount > 0 ? ", " : "")
                + saleCount + " buy" + (saleCount > 1 ? "s" : "")

        // add the total
        summary +=
            ": " + this.currencyPipe.transform(this.getTotalPrice(), "USD")
        return summary
    }

    /**
     * Returns the total shipping amount to be charged for the items
     * in this cart.
     */
    getTotalShipping() {
        // calculate the amount from the rental videos as $14
        // plus $2 per video beyond seven
        const rentalsCount = this.rentalVideos.length
        const rentalsAmount =
            (rentalsCount == 0) ? 0 : Math.max(14, rentalsCount * 2)

        // calculate the amount from the for-sale videos
        const salesCount = this.forSaleVideos.length
        const salesAmount =
            (salesCount == 0) ? 0 : (salesCount <= 10) ? 10 : 20

        // combine the two amounts to get the total
        return salesAmount + rentalsAmount
    }

    /**
     * Returns the final, total price of the items in this cart,
     * including the amount of credit applied, the tax, and the shipping amount.
     */
    getFinalPrice(state: string) {
        return this.getTotalPrice() - this.getCreditToApply()
            + this.getTotalTax(state) + this.getTotalShipping()
    }

    /**
     * Returns the total tax to be charged on the items in this cart,
     * based upon the given U.S. state (abbreviation).
     */
    getTotalTax(state: string) {
        if (state !== "CA") return 0
        const taxRate = .07
        return (this.getTotalPrice() - this.getCreditToApply()) * taxRate
    }

    /**
     * Clears this cart of the kind of videos not selected for perusal,
     * as Cyrus wants customers to select from only one kind per order.
     */
    removeVideosOfTypeNotSelected() {
        const videos = this.isRenting ? this.forSaleVideos : this.rentalVideos
        videos.forEach(video => this.removeVideo(video))
    }

    /**
     * Returns what the (whole, with no fraction) due date would be for videos rented now.
     */
    getRentalDueDate(): Date {
        const rentalDurationInDays = 14
        const date = new Date()
        date.setHours(0, 0, 0, 0)
        date.setDate(date.getDate() + rentalDurationInDays)
        return date
    }
}
