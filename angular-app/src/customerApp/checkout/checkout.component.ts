import {Component, OnInit} from "@angular/core"
import {Store} from "@ngrx/store"
import {CartService} from "customerApp/cart/cart.service"
import {prices} from "shared/Constants"
import {
    selectCustomer,
    selectCustomerIsRenting
} from "customerApp/customer/customer.reducer"
import {areCustomerFieldsValid, Customer} from "shared/models/Customer"
import {rentalAgreement} from "./rentalAgreement"
import {OrderPlacementService} from "../orders/order-placement.service"

@Component({
    selector: "customer-checkout",
    templateUrl: "./checkout.component.html",
    styleUrls: ["./checkout.component.scss"]
})
export class CheckoutComponent implements OnInit {
    customer: Customer

    isRenting: boolean

    prices = prices

    paymentToken: object

    rentalAgreement = rentalAgreement

    constructor(
        public cart: CartService,
        private orderPlacementService: OrderPlacementService,
        private store: Store) {
        store.select(selectCustomer).subscribe(customer => this.customer = {...customer})
        store.select(selectCustomerIsRenting).subscribe(value => this.isRenting = value)
    }

    isCustomerInfoValid(): boolean {
        return areCustomerFieldsValid(this.customer)
    }

    ngOnInit() {
        this.loadStripe()
    }

    // taken from https://www.tutsmake.com/angular-11-stripe-payment-checkout-gateway-example/
    loadStripe() {
        if (!window.document.getElementById('stripe-script')) {
            const s = window.document.createElement("script");
            s.id = "stripe-script";
            s.type = "text/javascript";
            s.src = "https://checkout.stripe.com/checkout.js";
            window.document.body.appendChild(s);
        }
    }

    // taken from https://www.tutsmake.com/angular-11-stripe-payment-checkout-gateway-example/
    displayPaymentDialog() {
        const handler = (<any>window).StripeCheckout.configure({
            key: 'pk_test_aeUUjYYcx4XNfKVW60pmHTtI',
            image: "assets/images/dvd-disc.png",
            locale: 'auto',
            token: (token) => {this.paymentToken = token}
        });

        handler.open({
            name: "Discount Video",
            description: "Specify card info for order",
            email: this.customer.emailAddress,
            amount: this.cart.getFinalPrice(this.customer.state) * 100
        });
    }

    placeOrder() {
        this.orderPlacementService.placeOrder(this.customer, this.paymentToken,
            this.isRenting)
        this.paymentToken = null
    }
}
