import {Injectable} from "@angular/core"
import {CanActivate, Router} from "@angular/router"
import {CartService} from "customerApp/cart/cart.service"

@Injectable()
export class CheckoutGuard implements CanActivate {
    constructor(private cart: CartService, private router: Router) {}

    canActivate() {
        return !this.cart.isEmpty() || this.router.parseUrl("/app/cart")
    }
}