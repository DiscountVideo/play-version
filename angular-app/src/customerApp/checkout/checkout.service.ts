import {catchError} from 'rxjs/operators'
import {Injectable} from "@angular/core"
import {HttpClient} from "@angular/common/http"
import {AuthService} from "../../shared/auth.service"
import {ErrorHandler} from "../../shared/error-handler.service"
import {Observable} from "rxjs"
import {serverUrl} from "../../shared/Constants"

@Injectable()
export class CheckoutService {
    constructor(
        private http: HttpClient, private auth: AuthService,
        private onError: ErrorHandler) {}

    private readonly options = () => ({headers: this.auth.httpHeaders})

    makeCharge(emailAddress: string, amount: number,
        paymentToken: object): Observable<{}> {
        const url = `${serverUrl}api/checkout${emailAddress}/${amount}`
        return this.http.post(
            url, paymentToken, {...this.options(), responseType: 'text'}).pipe(
            catchError(error => this.onError.alert("Could not make charge", error)))
    }
}


