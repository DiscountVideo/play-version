export const rentalAgreement =
    "I understand that if I return DVDs rented from DI$COUNT VIDEO in damaged" +
    " condition or if I fail to return them, I will be charged the full retail" +
    " replacement price (up to $100 per DVD) and I hereby authorize such charges" +
    " to the credit card indicated or to any other credit card I may use" +
    " to obtain merchandise from DI$COUNT VIDEO in the future.\n\n" +
    "I understand that if I do not return DVDs rented from DI$COUNT VIDEO" +
    " within seven calendar days of receiving them, my credit card will" +
    " be charged a late fee.  I understand that the late fee is $1 per DVD" +
    " per day for every day I keep the DVDs past the expiration of their" +
    " seven-day rental period until I send them back."
