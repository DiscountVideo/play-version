import {Component} from "@angular/core"
import {SessionRestorationService} from "./session-restoration.service"
import {Router} from "@angular/router"

@Component({
    selector: 'customer-app',
    templateUrl: './customer-app.component.html',
    styleUrls: ['./customer-app.component.css']
})
export class CustomerAppComponent {
    constructor(public router: Router,
        private sessionRestoration: SessionRestorationService) {
        sessionRestoration.restoreSession()
    }
}
