import {CommonModule, CurrencyPipe} from "@angular/common"
import {HttpClientModule} from "@angular/common/http"
import {NgModule} from "@angular/core"
import {FormsModule} from "@angular/forms"
import {RouterModule} from "@angular/router"
import {NgbModule} from "@ng-bootstrap/ng-bootstrap"
import {StoreModule} from "@ngrx/store"
import {CustomersService} from "shared/customers/customers.service"
import {OrdersService} from "shared/orders/orders.service"
import {SharedModule} from "shared/shared.module"
import {VideosService} from "shared/videos/videos.service"
import {cartReducer} from "customerApp/cart/cart.reducer"
import {CheckoutGuard} from "customerApp/checkout/checkout.guard"
import {customerReducer} from "customerApp/customer/customer.reducer"
import {CustomerAppRoutes} from "customerApp/customer-app.routes"
import {NgxPaginationModule} from "ngx-pagination"
import {CartService} from "customerApp/cart/cart.service"
import {CartComponent} from "./cart/cart.component"
import {CartVideosService} from "./cart/cart-videos.service"
import {CheckoutComponent} from "./checkout/checkout.component"
import {CheckoutService} from "./checkout/checkout.service"
import {ConsignmentComponent} from "./customer/consignment.component"
import {CustomerAppComponent} from "./customer-app.component"
import {ChangePasswordComponent} from "./login/change-password.component"
import {CreateAccountComponent} from "./login/create-account.component"
import {LoginComponent} from "./login/login.component"
import {LogOuterService} from "./login/log-outer.service"
import {NavBarComponent} from "./nav-bar.component"
import {StartComponent} from "./start.component"
import {VideoComponent} from "./videos/video.component"
import {VideosComponent} from "./videos/videos.component"
import {VideoStatusButtonComponent} from "./videos/video-status-button.component"
import {OrderPlacementService} from "./orders/order-placement.service"
import {OrderCompleteComponent} from "./orders/order-complete.component"
import {LoginService} from "./login/login.service"
import {SessionRestorationService} from "./session-restoration.service"
import {RentedVideosComponent} from "./customer/rented-videos.component";
import {RentedVideosTableComponent} from "./customer/rented-videos-table.component";

@NgModule({
    declarations: [
        CustomerAppComponent,
        StartComponent,
        VideosComponent,
        VideoComponent,
        VideoStatusButtonComponent,
        CartComponent,
        NavBarComponent,
        LoginComponent,
        CheckoutComponent,
        OrderCompleteComponent,
        ChangePasswordComponent,
        ConsignmentComponent,
        CreateAccountComponent,
        RentedVideosComponent,
        RentedVideosTableComponent
    ],
    exports: [],
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        NgbModule,
        NgxPaginationModule,
        SharedModule,
        RouterModule.forChild(CustomerAppRoutes),
        StoreModule.forRoot({
            cartState: cartReducer, customerState: customerReducer
        }, {
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
                strictStateSerializability: false,
                strictActionSerializability: false,
                strictActionWithinNgZone: true
            }
        })
    ],
    providers: [CartService, LoginService, LogOuterService, CustomersService,
        CartVideosService, CurrencyPipe, SessionRestorationService,
        CheckoutService, CheckoutGuard, OrderPlacementService, OrdersService, VideosService],
    entryComponents: [VideoComponent]
})
export class CustomerAppModule {
}
