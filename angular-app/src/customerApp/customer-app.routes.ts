import {AuthGuard} from "shared/auth.guard"
import {CartComponent} from "customerApp/cart/cart.component"
import {CheckoutComponent} from "customerApp/checkout/checkout.component"
import {CheckoutGuard} from "customerApp/checkout/checkout.guard"
import {ConsignmentComponent} from "customerApp/customer/consignment.component"
import {CustomerAppComponent} from "customerApp/customer-app.component"
import {ChangePasswordComponent} from "customerApp/login/change-password.component"
import {CreateAccountComponent} from "customerApp/login/create-account.component"
import {LoginComponent} from "customerApp/login/login.component"
import {StartComponent} from "customerApp/start.component"
import {VideosComponent} from "customerApp/videos/videos.component"
import {OrderCompleteComponent} from "./orders/order-complete.component"
import {Routes} from "@angular/router";
import {RentedVideosComponent} from "./customer/rented-videos.component";

export const CustomerAppRoutes: Routes = [{
    path: "",
    component: CustomerAppComponent,
    children: [
        {path: "", component: StartComponent},
        {path: "login", component: LoginComponent},
        {path: "videos", component: VideosComponent},
        {
            path: "cart",
            component: CartComponent,
            canActivate: [AuthGuard]
        },
        {
            path: "checkout",
            component: CheckoutComponent,
            canActivate: [AuthGuard, CheckoutGuard]
        },
        {
            path: "orderComplete/:orderId",
            component: OrderCompleteComponent,
            canActivate: [AuthGuard]
        },
        {
            path: "changePassword", component: ChangePasswordComponent,
            canActivate: [AuthGuard]
        },
        {
            path: "rentedVideos", component: RentedVideosComponent,
            canActivate: [AuthGuard]
        },
        {
            path: "consignment", component: ConsignmentComponent,
            canActivate: [AuthGuard]
        },
        {path: "createAccount", component: CreateAccountComponent}
    ]
}]