import {CartState} from "customerApp/cart/cart.reducer"
import {CustomerState} from "customerApp/customer/customer.reducer"
import {createFeatureSelector} from "@ngrx/store"

export interface CustomerAppState {
    cartState: CartState
    customerState: CustomerState
}

export const selectCartState = createFeatureSelector<CustomerAppState, CartState>(
    "cartState")

export const selectCustomerState = createFeatureSelector<CustomerAppState, CustomerState>(
    "customerState")
