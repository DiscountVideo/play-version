import {Component} from "@angular/core"
import {CartService} from "customerApp/cart/cart.service"
import {Customer} from "../../shared/models/Customer"
import {Store} from "@ngrx/store"
import {selectCustomer} from "./customer.reducer"

@Component({
    selector: 'customer-consignment',
    templateUrl: './consignment.component.html'
})
export class ConsignmentComponent {
    customer: Customer

    constructor(private store: Store, public cart: CartService) {
        store.select(selectCustomer).subscribe(customer => this.customer = customer)
    }
}
