import {createAction, props} from "@ngrx/store"
import {Customer} from "shared/models/Customer"

export const CustomerActions = {
    retrieval: createAction("[Customer] Retrieval", props<{ customer: Customer }>()),
    isRentingDecision: createAction("[Customer] Is-renting decision",
        props<{ isRenting: boolean }>()),
    login: createAction("[Customer] Login", props<{ customer: Customer }>()),
    logout: createAction("[Customer] Logout"),
    update: createAction("[Customer] Update", props<{ customer: Customer }>())
}