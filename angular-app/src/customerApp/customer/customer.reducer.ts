import {Action, createReducer, createSelector, on} from "@ngrx/store"
import {Customer} from "shared/models/Customer"
import {selectCustomerState} from "customerApp/customer-app.state"
import {CustomerActions} from "customerApp/customer/customer.actions"

export interface CustomerState {
    customer: Customer
    isRenting: boolean
}

export const initialCustomerState: CustomerState = {
    customer: null,
    isRenting: false
}

export const selectCustomer = createSelector(selectCustomerState,
    (state: CustomerState) => state.customer)
export const selectCustomerIsRenting = createSelector(selectCustomerState,
    (state: CustomerState) => state.isRenting)

const CustomerReducer = createReducer(initialCustomerState,
    on(CustomerActions.retrieval, CustomerActions.login, CustomerActions.update,
        (state, {customer}) => ({...state, customer: customer})),
    on(CustomerActions.isRentingDecision,
        (state, {isRenting}) => ({...state, isRenting})),
    on(CustomerActions.logout, state => ({...state, customer: null}))
)

export function customerReducer(state: CustomerState | undefined, action: Action) {
    return CustomerReducer(state, action)
}