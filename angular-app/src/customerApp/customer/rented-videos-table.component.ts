import {Component, Input} from "@angular/core"
import {RentedVideo} from "./rented-videos.component";

@Component({
    selector: 'dv-rented-videos-table',
    templateUrl: './rented-videos-table.component.html',
    styleUrls: ['./rented-videos-table.component.css']
})
export class RentedVideosTableComponent {
    now = new Date()

    @Input() isForCurrent: boolean

    @Input() videos: RentedVideo[]
}
