import {Component, OnInit} from "@angular/core"
import {Store} from "@ngrx/store"
import {selectCustomer} from "./customer.reducer"
import {OrdersService} from "../../shared/orders/orders.service";
import * as _ from "lodash";

export interface RentedVideo {
    title: string
    rentalDate: Date
    dueDate: Date
    isShipped: boolean
}

@Component({
    selector: 'dv-rented-videos',
    templateUrl: './rented-videos.component.html'
})
export class RentedVideosComponent implements OnInit {
    currentVideos: RentedVideo[]
    pastVideos: RentedVideo[]

    constructor(private store: Store, public ordersService: OrdersService) { }

    ngOnInit() {
        this.store.select(selectCustomer).subscribe(customer => {
            if (!customer) return;
            this.ordersService.fetchRentalOrdersForCustomer(customer._id)
                .subscribe(orders => {
                    this.currentVideos = _.flatMap(orders, order => {
                        return order.videos.filter(video => !video.isReturned).map(video => ({
                            title: video.title,
                            rentalDate: order.date,
                            dueDate: order.dueDate,
                            isShipped: order.isShipped
                        } as RentedVideo))
                    })
                    this.pastVideos = _.flatMap(orders, order => {
                        return order.videos.filter(video => video.isReturned).map(video => ({
                            title: video.title,
                            rentalDate: order.date,
                        } as RentedVideo))
                    })
                })
        })
    }
}
