import {Component} from "@angular/core"
import {Store} from "@ngrx/store"
import {Customer} from "shared/models/Customer"
import {CustomersService} from "shared/customers/customers.service"
import {selectCustomer} from "customerApp/customer/customer.reducer"

@Component({
    selector: "customer-change-password",
    templateUrl: "./change-password.component.html",
    styleUrls: ["./change-password.component.scss"]
})
export class ChangePasswordComponent {
    private customer: Customer

    constructor(private store: Store,
        private customersService: CustomersService) {
        store.select(selectCustomer).subscribe(customer => this.customer = customer)
    }

    makeChange = (newPassword: string) => {
        return this.customersService.changeCustomerPassword(this.customer._id,
            newPassword)
    }
}