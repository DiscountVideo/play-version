import {Component} from "@angular/core"
import {isEmailAddressValid} from "../../shared/Validators"
import {CustomersService} from "../../shared/customers/customers.service"

@Component({
    selector: 'customer-create-account',
    templateUrl: './create-account.component.html'
})
export class CreateAccountComponent {
    emailAddress: string
    isAddressValidated = false
    validationCode: string
    isAddressInUse = false
    validationCodeEntered: string
    isCodeInvalid = false
    isAccountCreated = false
    firstName: string
    lastName: string
    password: string
    reenteredPassword: string

    constructor(private customersService: CustomersService) {}

    shouldDisableSubmitEmailAddressButton(): boolean {
        return !isEmailAddressValid(this.emailAddress)
    }

    validateEmailAddress() {
        // if the server considers the new address valid
        this.isAddressInUse = false
        this.customersService.validateEmailAddress(this.emailAddress)
            .subscribe(
                code => {
                    // store the validation code sent by the server, so we
                    // can compare it to the value entered by the user
                    this.validationCode = code
                },
                error => {
                    if (error.error && error.error.indexOf("already has") > 0)
                        this.isAddressInUse = true
                }
            )
    }

    onValidationCodeSubmit() {
        // if the code matches that we got from the server, the e-mail address
        // has been validated
        if (this.validationCodeEntered == this.validationCode)
            this.isAddressValidated = true

        // otherwise, the code entered is invalid
        else this.isCodeInvalid = true
    }

    shouldDisableCreateAccountButton() {
        return !this.firstName || !this.lastName
            || !this.reenteredPassword || this.reenteredPassword.length < 5
            || !this.password || this.password.length < 5
            || this.reenteredPassword !== this.password
    }

    createAccount() {
        // when the server has created the account
        this.customersService.createCustomer(
            this.firstName, this.lastName, this.emailAddress, this.password)
            .subscribe(ignore => {
                // transmit that fact to the view
                this.isAccountCreated = true
            })
    }
}

