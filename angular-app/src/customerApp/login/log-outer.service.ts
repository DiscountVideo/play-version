import {Injectable} from "@angular/core"
import {Store} from "@ngrx/store"
import {AuthService} from "shared/auth.service"
import {CartService} from "customerApp/cart/cart.service"
import {Router} from "@angular/router"
import {CustomerActions} from "customerApp/customer/customer.actions"

@Injectable()
export class LogOuterService {
    private loginStillValidIntervalId = -1

    constructor(private auth: AuthService, private cart: CartService,
        private router: Router, private store: Store) {}

    logOut() {
        // stop checking at intervals to determine if the token is still valid
        if (this.loginStillValidIntervalId >= 0)
            window.clearInterval(this.loginStillValidIntervalId)

        this.auth.logOut()
        this.store.dispatch(CustomerActions.logout())
        this.router.navigateByUrl('/app/login').then()
    }

    startTokenExpirationChecks() {
        // institute a check at intervals whether the token is still valid,
        // logging the user out when it isn't
        this.loginStillValidIntervalId =
            window.setInterval(
                () => {if (!this.auth.isLoggedIn) this.logOut()},
                10 * 60 * 1000
            )
    }
}