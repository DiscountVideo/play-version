import {Component} from "@angular/core"
import {Store} from "@ngrx/store"
import {AuthService} from "shared/auth.service"
import {CustomersService} from "shared/customers/customers.service"
import {Observable} from "rxjs"
import {map} from "rxjs/operators"
import {CartService} from "customerApp/cart/cart.service"
import {LoginService} from "./login.service"

@Component({
    selector: 'customer-login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    emailAddress: string
    isPasswordMessageSent = false
    substituteCode: string
    isSubstituteCodeInvalid = false

    constructor(
        private auth: AuthService, private cart: CartService,
        private store: Store,
        private customersService: CustomersService,
        private loginService: LoginService) {}

    logIn = (emailAddress: string, password: string): Observable<boolean> => {
        return this.customersService.loginCustomer(emailAddress, password).pipe(
            map(result => {
                if (result.token) this.loginService.handleLogin(result.customer,
                    result.token)
                return !result.errorMessage
            }))
    }

    shouldDisableForgotPasswordButton(): boolean {
        return !this.emailAddress || this.emailAddress.length == 0
            || this.isPasswordMessageSent
    }

    onForgotPasswordClick = () => {
        // if the server decides the supplied e-mail address is one associated
        // with a customer account
        this.customersService.notifyPasswordForgotten(this.emailAddress)
            .subscribe(ignore => {
                // the server has sent a message to the supplied e-mail address
                // which contains a substitute login code
                this.isPasswordMessageSent = true
            })
    }

    onValidateSubstituteCodeClick() {
        // if the server validates the substitute login code
        this.isSubstituteCodeInvalid = false
        this.customersService.validateSubstituteCode(
            this.substituteCode, this.emailAddress)
            .subscribe(
                result => {
                    if (result.errorMessage) this.isSubstituteCodeInvalid = true

                    // the user is now logged in
                    else this.loginService.handleLogin(result.customer, result.token)
                },
                () => this.isSubstituteCodeInvalid = true
            )
    }
}

