import {Injectable} from "@angular/core"
import {Customer} from "../../shared/models/Customer"
import {keys} from "../../shared/Constants"
import {CustomerActions} from "../customer/customer.actions"
import {AuthService} from "../../shared/auth.service"
import {CartService} from "../cart/cart.service"
import {Store} from "@ngrx/store"
import {Router} from "@angular/router"
import {LogOuterService} from "./log-outer.service"

@Injectable()
export class LoginService {
    constructor(
        private auth: AuthService, private cart: CartService,
        private store: Store,
        private router: Router, private logOuter: LogOuterService) {}

    handleLogin(customer: Customer, token: string) {
        // save the JWT supplied by the server, to be included in
        // all future web API calls
        this.auth.saveToken(token)

        this.logOuter.startTokenExpirationChecks()

        // store in local storage that the customer is logged in,
        // so we may automatically re-login the customer in after
        // any page refresh
        window.localStorage[keys.customerId] = customer._id

        // refresh the customer's cart, then remove any videos from it that are
        // not of the kind selected for perusal
        this.store.dispatch(CustomerActions.login({customer}))
        this.cart.refresh()
            .subscribe(ignore => this.cart.removeVideosOfTypeNotSelected())

        // start the logged-in user on the videos page
        this.router.navigateByUrl('/app/videos').then()
    }
}