import {Component} from '@angular/core'
import {Router} from "@angular/router"
import {AuthService} from "../shared/auth.service"
import {CartService} from "customerApp/cart/cart.service"
import {LogOuterService} from "./login/log-outer.service"
import {Customer} from "../shared/models/Customer"
import {Store} from "@ngrx/store"
import {selectCustomer, selectCustomerIsRenting} from "./customer/customer.reducer"

@Component({
    selector: 'customer-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {
    customer: Customer

    isRenting: boolean

    constructor(private store: Store,
        public router: Router, public auth: AuthService,
        public cart: CartService, public logOuter: LogOuterService) {
        store.select(selectCustomer).subscribe(customer => this.customer = customer)
        store.select(selectCustomerIsRenting)
            .subscribe(value => this.isRenting = value)
    }
}
