import {Component, OnInit} from "@angular/core"
import {Order} from "../../shared/models/Order"
import {ActivatedRoute} from "@angular/router"
import {OrdersService} from "../../shared/orders/orders.service"

@Component({
    selector: "customer-order-complete",
    templateUrl: "./order-complete.component.html"
})
export class OrderCompleteComponent implements OnInit {
    order: Order

    hasRentals: boolean

    constructor(private route: ActivatedRoute,
        private ordersService: OrdersService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => this.loadOrder(params["orderId"]))
    }

    private loadOrder(orderId: string) {
        this.ordersService.fetchOrder(orderId)
            .subscribe(order => {
                this.order = order
                this.hasRentals = order.rentalsDueDate != null
            })
    }
}