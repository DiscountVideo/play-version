import {Injectable} from "@angular/core"
import {CartService} from "../cart/cart.service"
import {CheckoutService} from "../checkout/checkout.service"
import {Router} from "@angular/router"
import {Store} from "@ngrx/store"
import {prices} from "shared/Constants"
import {Customer} from "../../shared/models/Customer"
import {CustomerActions} from "../customer/customer.actions"
import {CartActions} from "../cart/cart.actions"
import {Observable} from "rxjs"
import {Order, OrderVideo} from "../../shared/models/Order"
import {CustomersService} from "../../shared/customers/customers.service"
import {OrdersService} from "../../shared/orders/orders.service"

@Injectable()
export class OrderPlacementService {
    prices = prices

    constructor(
        private cart: CartService,
        private customersService: CustomersService,
        private checkoutService: CheckoutService,
        private ordersService: OrdersService,
        private router: Router, private store: Store,
    ) {}

    placeOrder(customer: Customer, paymentToken: object, isRenting: boolean) {
        this.createOrder(customer, isRenting).subscribe(orderId => {
            this.makeCharge(customer, paymentToken)

            this.customersService.updateCustomer(customer).subscribe(
                () => this.store.dispatch(CustomerActions.update({customer}))
            )

            this.store.dispatch(CartActions.orderCompletion())

            // go to the completed order page
            this.router.navigate(["/app/orderComplete", orderId]).then()
        })
    }

    /**
     * Has the server create an order from the data gathered during checkout.
     */
    private createOrder(customer: Customer, isRenting: boolean): Observable<string> {
        // create an order object from the data gathered during checkout
        const cart = this.cart
        const state = customer.state
        const order = {
            _id: null,
            date: new Date(),
            customer,
            videos: null,
            rentalsDueDate: isRenting ? cart.getRentalDueDate() : null,
            productsTotal: cart.getTotalPrice(),
            creditApplied: cart.getCreditToApply(),
            tax: cart.getTotalTax(state),
            shippingTotal: cart.getTotalShipping(),
            finalPrice: cart.getFinalPrice(state),
            isShipped: false
        } as Order

        // include in the order object the info for the videos in the cart
        order.videos =
            this.cart.getAllVideos().map(v => ({
                    _id: v._id,
                    code: v.code,
                    title: v.title,
                    isRental: v.isRental,
                    price: v.isRental ? prices.rental : prices.forSale,
                    isReturned: false
                } as OrderVideo)
            )

        return this.ordersService.addOrder(order)
    }

    private makeCharge(customer: Customer, paymentToken: object) {
        const amount = Math.round(this.cart.getFinalPrice(customer.state) * 100)
        this.checkoutService.makeCharge(customer.emailAddress, amount, paymentToken)
    }
}