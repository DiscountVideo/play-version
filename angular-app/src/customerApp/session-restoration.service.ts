import {Injectable} from "@angular/core"
import {AuthService} from "../shared/auth.service"
import {CartService} from "./cart/cart.service"
import {ErrorHandler} from "../shared/error-handler.service"
import {LogOuterService} from "./login/log-outer.service"
import {CustomersService} from "../shared/customers/customers.service"
import {Store} from "@ngrx/store"
import {CustomerActions} from "./customer/customer.actions"
import {selectCustomerIsRenting} from "./customer/customer.reducer"
import {keys} from "../shared/Constants"

@Injectable()
export class SessionRestorationService {
    constructor(
        private auth: AuthService, private cart: CartService,
        private onError: ErrorHandler,
        private logOuter: LogOuterService, private customersService: CustomersService,
        private store: Store) {}

    restoreSession(): void {
        // set the initial is-for-renting status to the value (if any) loaded from the
        // browser's storage
        const isRenting = window.localStorage.getItem('isForRenting') == 'true'
        this.store.dispatch(CustomerActions.isRentingDecision({isRenting}))

        // persist changes of the is-for-renting status to the browser's storage
        this.store.select(selectCustomerIsRenting).subscribe(value =>
            window.localStorage.setItem('isForRenting', value.toString()))

        // if there is still a customer logged in from the previous session,
        // try to reload it
        if (this.auth.isLoggedIn()) this.fetchCustomer()
    }

    /**
     * Tries to fetch from the server the customer whose ID has been stored
     * in local storage as the last one to be logged into the app from
     * the user's machine.  If the customer is fetched, it is considered to
     * still be logged in.
     */
    private fetchCustomer() {
        // if we can reload the customer's info from the server
        const id = window.localStorage.getItem(keys.customerId)
        this.customersService.fetchCustomer(id)
            .subscribe(
                customer => {
                    // skip the login page, as the customer is now logged in again
                    this.store.dispatch(CustomerActions.retrieval({customer}))
                    this.logOuter.startTokenExpirationChecks()
                    this.cart.refresh().subscribe()
                },
                error => {
                    this.onError.alert("Could not reload customer info", error)
                    this.logOuter.logOut()
                }
            )
    }
}