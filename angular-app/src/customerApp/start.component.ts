import {Component} from "@angular/core"
import {Router} from "@angular/router"
import {CartService} from "customerApp/cart/cart.service"
import {Store} from "@ngrx/store"
import {CustomerActions} from "./customer/customer.actions"
import {terms} from "./terms"
import {ageWarning1, ageWarning2} from "./ageWarnings"

@Component({
    selector: 'start',
    templateUrl: './start.component.html',
    styleUrls: ['./start.component.scss']
})
export class StartComponent {
    areTermsAgreedTo = false

    window = window

    ageWarning1 = ageWarning1

    ageWarning2 = ageWarning2

    terms = terms

    constructor(private store: Store, private router: Router,
        private cart: CartService) {}

    onViewVideosClicked(shouldViewRentals: boolean) {
        this.store.dispatch(
            CustomerActions.isRentingDecision({isRenting: shouldViewRentals}))

        this.cart.removeVideosOfTypeNotSelected()

        this.router.navigateByUrl('/app/videos').then()
    }
}