export const terms =
    "I hereby certify:\n" +
    "That I am at least 18 years of age or of legal age in my area" +
    " to view adult oriented material and that I am entering this website" +
    " with full knowledge that it contains adult oriented material.\n" +
    "That, to the best of my knowledge, the viewing, reading, and" +
    " downloading of the images in this website do not violate the" +
    " standards of my community, village, city, town, county, state," +
    " province or country.\n" +
    "That the sexually explicit material I am accessing here is for" +
    " my own personal use and isn't to be viewed by minors or anyone" +
    " else but myself.\n" +
    "That I will not permit minors to view this material at any time.\n" +
    "That I am not acting on behalf of the U.S. Postal Service or any" +
    " other government law enforcement agency or any religious or" +
    " censorship group, and am not hereby attempting to obtain evidence" +
    " for the prosecution of any individual or corporation or for the" +
    " purpose of entrapment.\n" +
    "That I desire to view and have not and will not notify any" +
    " governmental agency to intercept sexually explicit material" +
    " on my behalf.\n" +
    "That I realize I can stop receiving such material at any time" +
    " by logging off now and not returning.\n" +
    "That I believe that as an adult, I have the inalienable right" +
    " to read and/or view any type of material that I choose.\n" +
    "And that I am, and agree to be, held wholly liable for any" +
    " false disclosures, and responsible for any legal ramifications" +
    " that may arise from viewing, reading, or downloading of images" +
    " or material contained within this website.\n\n" +
    "(By confirming this agreement and/or by looking at any of the" +
    " pictures on this site you are agreeing that the pictures contained" +
    " herein are not obscene or offensive in any way nor could ever be" +
    " construed to be so. You are also certifying that the material" +
    " presented here is not illegal or considered obscene in your street," +
    " village, community, city, state, province or country. If you are" +
    " unsure, do not continue.)\n\n" +
    "Site owner has no responsibility towards server crashes, computer" +
    " malfunctions, errors in transmission, or any other technical" +
    " problems that would unlikely arise. Members shall not hold the" +
    " site owner liable for any sort of discomfort or aggravations." +
    " Your computer's ability to view the images is your responsibility." +
    " If you don't feel you get to see any particular body part in any" +
    " particular state of function enough, go back and read the second" +
    " sentence. This site is intended for fun and entertainment" +
    " purposes only.\n\n" +
    "You MUST agree with ALL of the above to view this site."

