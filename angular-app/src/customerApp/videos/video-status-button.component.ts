import {Component, Input} from "@angular/core"
import {Video} from "../../shared/models/Video"
import {videoStatuses} from "../../shared/videos/VideoStatuses"
import {CartService} from "customerApp/cart/cart.service"
import {Customer} from "../../shared/models/Customer"
import {selectCustomer} from "../customer/customer.reducer"
import {Store} from "@ngrx/store"

@Component({
    selector: "customer-video-status-button",
    templateUrl: "./video-status-button.component.html"
})
export class VideoStatusButtonComponent {
    @Input() video: Video

    videoStatuses = videoStatuses

    customer: Customer

    constructor(private store: Store, public cart: CartService) {
        store.select(selectCustomer).subscribe(customer => this.customer = customer)
    }

    getDueBackDate(video: Video): Date {
        const rentalDurationInDays = 14
        const msInOneDay = 24 * 60 * 60 * 1000
        const dueDateMs = video.statusDate.getTime() + rentalDurationInDays *
            msInOneDay
        return new Date(dueDateMs)
    }
}
