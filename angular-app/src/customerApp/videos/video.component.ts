import {Component} from "@angular/core"
import {Video} from "shared/models/Video"
import {prices} from "shared/Constants"
import {BsModalRef} from "ngx-bootstrap/modal"

@Component({
    selector: "customer-video",
    templateUrl: "./video.component.html"
})
export class VideoComponent {
    video: Video

    prices = prices

    constructor(private bsModalRef: BsModalRef) {
    }

    onCloseClick = () => {
        this.bsModalRef.hide()
    }
}
