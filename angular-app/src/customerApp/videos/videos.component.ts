import {Component, OnInit} from "@angular/core"
import {VideosService} from "shared/videos/videos.service"
import {Video} from "shared/models/Video"
import {noFirstLetterFilterValue, prices} from "shared/Constants"
import {VideoComponent} from "./video.component"
import {BsModalService} from "ngx-bootstrap/modal"
import {selectCustomerIsRenting} from "../customer/customer.reducer"
import {Store} from "@ngrx/store"

@Component({
    selector: "customer-videos",
    templateUrl: "./videos.component.html"
})
export class VideosComponent implements OnInit {
    prices = prices

    isRenting: boolean

    videos: Video[]
    totalCount: number
    pageNumber = searchSettings.pageNumber
    videosPerPage = searchSettings.videosPerPage

    firstLetter = searchSettings.firstLetter
    titleSearchText = searchSettings.titleSearchText
    codeSearchText: string = null
    searchTag = searchSettings.searchTag
    searchStudio = searchSettings.searchStudio

    tags: string[]
    studios: string[]

    constructor(
        private videosService: VideosService,
        private store: Store,
        private modalService: BsModalService) {
    }

    ngOnInit(): void {
        this.store.select(selectCustomerIsRenting).subscribe(value => {
            this.isRenting = value
            this.codeSearchText = value ? searchSettings.codeSearchText : null
        })

        this.fetchVideosPage()
        this.fetchTags()
        this.fetchStudios()
    }

    onPageChange = (number: number) => {
        this.pageNumber = searchSettings.pageNumber = number - 1
        this.fetchVideosPage()
    }

    onFirstLetterSelect = (letter: string) => {
        this.firstLetter = searchSettings.firstLetter = letter
        this.pageNumber = 0
        this.fetchVideosPage()
    }

    onVideosPerPageSelect = (numVideos: number) => {
        this.videosPerPage = searchSettings.videosPerPage = numVideos
        this.pageNumber = 0
        this.fetchVideosPage()
    }

    onTitleSearchTextChange = (value: string) => {
        this.titleSearchText = searchSettings.titleSearchText = value
        this.pageNumber = 0
        this.fetchVideosPage()
    }

    onCodeSearchTextChange = (value: string) => {
        this.codeSearchText = searchSettings.codeSearchText = value
        this.pageNumber = 0
        this.fetchVideosPage()
    }

    onTagSelect = (tag: string) => {
        this.searchTag = searchSettings.searchTag = tag
        this.pageNumber = 0
        this.fetchVideosPage()
    }

    onStudioSelect = (studio: string) => {
        this.searchStudio = searchSettings.searchStudio = studio
        this.pageNumber = 0
        this.fetchVideosPage()
    }

    onVideoClick = (video: Video) => {
        this.modalService.show(VideoComponent, {initialState: {video}})
    }

    private fetchVideosPage() {
        this.videosService
            .fetchVideosPage(
                this.isRenting,
                this.pageNumber, this.firstLetter, this.videosPerPage,
                this.titleSearchText, this.searchTag, this.searchStudio,
                this.codeSearchText)
            .subscribe(result => {
                this.videos = result.videos
                this.totalCount = result.totalCount
            })
    }

    private fetchTags() {
        this.videosService.fetchTags(this.isRenting)
            .subscribe(tags => this.tags = tags)
    }

    private fetchStudios() {
        this.videosService.fetchStudios(this.isRenting)
            .subscribe(studios => this.studios = studios)
    }
}

interface SearchSettings {
    videosPerPage: number;
    titleSearchText?: string;
    codeSearchText?: string;
    firstLetter: string;
    pageNumber: number;
    searchTag?: string;
    searchStudio?: string;
}

// stores the settings which should persist across visits to this page
const searchSettings: SearchSettings = {
    videosPerPage: 10,
    firstLetter: noFirstLetterFilterValue,
    pageNumber: 0
}


