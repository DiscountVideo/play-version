export const
    serverUrl = "http://localhost:9000/",
    prices = {
        rental: 5,
        forSale: 9.95,
        shippingPerRental: 4
    },
    noFirstLetterFilterValue = '*',
    keys = {
        customerId: "customerId",
        adminId: "adminId"
    }
