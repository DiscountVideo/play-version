export function isZipCodeValid(zip: string): boolean {
    return zip && /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip)
}

export function isPhoneNumberValid(number: string): boolean {
    return number && /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/.test(number)
}

export function isEmailAddressValid(address: string): boolean {
    return address &&
        /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
            .test(address)
}
