import {Injectable} from "@angular/core"
import {isAdminAppInUse} from "./Globals"
import {HttpHeaders} from "@angular/common/http"

@Injectable()
export class AuthService {
    private token: string

    private tokenExpireTime: number

    httpHeaders: HttpHeaders

    isLoggedIn = () => this.getToken() && this.isLoginStillValid()

    private static getTokenKey(): string {
        return isAdminAppInUse ? "discount-video-admin-token" : "discount-video-token"
    }

    constructor() {
        this.token = AuthService.fetchTokenFromStorage()
        if (this.token) {
            this.createHeaders()
            this.storeTokenExpireTime()
        }
    }

    private isLoginStillValid(): boolean {
        // if there is no token expiration time, the user is not logged in
        if (!this.tokenExpireTime) return false

        // return whether the token has yet to expire
        return this.tokenExpireTime > (new Date().getTime() / 1000)
    }

    saveToken(token: string) {
        this.token = token
        window.localStorage.setItem(AuthService.getTokenKey(), token)

        this.createHeaders()
        this.storeTokenExpireTime()
    }

    private createHeaders() {
        const headers = {
            Authorization: 'Bearer ' + this.token
        }

        this.httpHeaders = new HttpHeaders(headers)
    }

    private storeTokenExpireTime() {
        const payload = JSON.parse(window.atob(this.token.split('.')[1]))
        this.tokenExpireTime = parseInt(payload.exp.toString())
    }

    private static fetchTokenFromStorage(): string {
        return window.localStorage.getItem(AuthService.getTokenKey())
    }

    getToken(): string {
        // if we have a token, then since calling this method indicates application usage,
        // extend the token's duration if we are getting near its expiration
        if (this.token) this.extendTokenDuration()

        return this.token
    }

    /**
     * Extends our token's duration if it is approaching its expiration.
     */
    private extendTokenDuration() {
        // TODO add this back in once we code to postpone the token's expiration
        // (or, create a new token) on the server side
        // const maxExtraMinutes = 20;
        // this.tokenExpireTime =
        //     Math.max(
        //         this.tokenExpireTime,
        //         new Date().getTime() / 1000 + maxExtraMinutes * 60
        //     );
    }

    logOut() {
        // perform the actual logout work
        this.token = null
        this.tokenExpireTime = null
        window.localStorage.removeItem(AuthService.getTokenKey())
    }
}
