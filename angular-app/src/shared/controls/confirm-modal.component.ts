import {Component} from "@angular/core"
import {BsModalRef} from "ngx-bootstrap/modal"

@Component({
    selector: "dv-confirm-modal",
    templateUrl: "./confirm-modal.component.html"
})
export class ConfirmModalComponent {
    /**
     * The fields below all get set by a config object provided to the modal service.
     */

    public title: String

    public text: String

    public shouldShowCancel: boolean

    private onChoiceMade: (confirmed: boolean) => void = () => {}

    constructor(private bsModalRef: BsModalRef) {}

    onOkClick = () => {
        this.bsModalRef.hide()
        this.onChoiceMade(true)
    }

    onCancelClick = () => {
        this.bsModalRef.hide()
        this.onChoiceMade(false)
    }
}
