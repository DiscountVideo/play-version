import {Component, Input} from '@angular/core'
import {noFirstLetterFilterValue} from "../Constants"

@Component({
    selector: 'dv-first-letter-filter',
    templateUrl: './first-letter-filter.component.html'
})
export class FirstLetterFilterComponent {
    @Input() currentLetter = noFirstLetterFilterValue

    letters = FirstLetterFilterComponent.createLetters()

    noFirstLetterFilterValue = noFirstLetterFilterValue

    onLetterClick = (letter: string) => {
        this.currentLetter = letter
        this.onLetterSelect(letter)
    }

    @Input() onLetterSelect: (letter: string) => void

    /**
     * Creates the array of letters (and otherwise) by which filtering may occur.
     */
    private static createLetters(): string[] {
        const letters = []
        letters[0] = noFirstLetterFilterValue
        letters[1] = '0'
        const aCode = 'A'.charCodeAt(0)
        for (let i = 0; i < 26; i++) letters[i + 2] = String.fromCharCode(aCode + i)
        return letters
    }
}