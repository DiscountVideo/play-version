import {Component, Input} from '@angular/core'

@Component({
    selector: 'dv-items-per-page-select',
    templateUrl: './items-per-page-select.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class ItemsPerPageSelectComponent {
    @Input() itemsPerPage: number

    @Input() onSelect: (numItems: number) => void

    itemsPerPageOptions = [5, 10, 15, 20, 30]
}
