import {Component, Input} from '@angular/core'

@Component({
    selector: 'dv-search-field',
    templateUrl: './search-field.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class SearchFieldComponent {
    @Input() domain: string

    @Input() searchText: string

    @Input() onSubmit: (value: string) => void

    onRemove() {
        this.searchText = null
        setTimeout(() => this.onSubmit(null))
    }

    onKeypress(event: KeyboardEvent) {
        if (event.code != "KeyEnter") return

        // perform a search as if the search button was clicked
        this.onSubmit(this.searchText)
        event.stopPropagation()
    }
}
