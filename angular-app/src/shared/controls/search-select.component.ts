import {Component, Input} from '@angular/core'

@Component({
    selector: 'dv-search-select',
    templateUrl: './search-select.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class SearchSelectComponent {
    @Input() domain: string

    @Input() width: string

    @Input() items: string[]

    @Input() currentItem: string

    @Input() onSelect: (item: string) => void

    onRemove = () => {
        this.currentItem = null
        this.onSelect(null)
    }
}
