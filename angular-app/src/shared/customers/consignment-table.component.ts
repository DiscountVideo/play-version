import {Component, Input} from "@angular/core"
import {Customer} from "../models/Customer"
import {videoStatuses} from "../videos/VideoStatuses"
import {Video} from "../models/Video"
import {VideosService} from "../videos/videos.service"
import * as _ from "lodash"

@Component({
    selector: 'dv-consignment-table',
    templateUrl: './consignment-table.component.html',
    providers: [VideosService]
})
export class ConsignmentTableComponent {
    customer: Customer

    @Input('customer') set setCustomer(customer: Customer) {
        if (!customer) return
        this.customer = customer
        this.fetchConsignmentVideos()
    }

    videoStatuses = videoStatuses

    videos: Video[]

    totalCredit = 0

    constructor(private videosService: VideosService) {}

    private fetchConsignmentVideos() {
        this.videosService.fetchConsignmentVideos(this.customer._id)
            .subscribe(videos => {
                this.videos = videos
                this.totalCredit = _.sum(videos.map(v => v.creditGiven))
            })
    }
}

