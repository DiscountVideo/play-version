import {Component, Input} from '@angular/core'
import {Customer} from "../models/Customer"
import {usStates} from "./UsStates"
import {isPhoneNumberValid, isZipCodeValid} from "../Validators"

@Component({
    selector: 'dv-customer-info-form',
    templateUrl: './customer-info-form.component.html'
})
export class CustomerInfoFormComponent {
    @Input() customer: Customer

    usStates = usStates

    isZipCodeValid(): boolean {
        return isZipCodeValid(this.customer.zipCode)
    }

    isPhoneNumberValid(): boolean {
        return isPhoneNumberValid(this.customer.phoneNumber)
    }
}
