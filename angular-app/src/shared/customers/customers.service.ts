import {catchError, map} from 'rxjs/operators'
import {Injectable} from "@angular/core"
import {serverUrl} from "../Constants"
import {ErrorHandler} from "../error-handler.service"
import {AuthService} from "../auth.service"
import {Observable} from "rxjs"
import {HttpClient} from "@angular/common/http"
import {Customer} from "../models/Customer"

@Injectable()
export class CustomersService {
    constructor(
        private http: HttpClient, private auth: AuthService, private onError: ErrorHandler) {}

    private readonly options = () => ({headers: this.auth.httpHeaders})

    fetchCustomer(id: string): Observable<Customer> {
        const url = `${serverUrl}api/customer/${id}`
        return this.http.get<Customer>(url, this.options()).pipe(
            catchError(error => this.onError.alert("Could not fetch customer", error)))
    }

    fetchCustomersPage(
        pageNumber: number, firstLetter: string, customersPerPage: number,
        lastNameSearchText: string): Observable<CustomersPageResult> {
        const url = `${serverUrl}api/customers`
        const params: any = {
            pageNumber,
            customersPerPage,
        }
        if (firstLetter != "*") params.firstLetter = firstLetter
        if (!!lastNameSearchText) params.lastNameSearchText = lastNameSearchText
        return this.http.get<CustomersPageResponse>(url, {...this.options(), params}).pipe(
            map(response => ({...response, totalCount: response.count})),
            catchError(
                error => this.onError.alert("Could not fetch customers page", error)))
    }

    fetchCustomerNames(): Observable<CustomerName[]> {
        const url = `${serverUrl}api/customers/names`
        return this.http.get<CustomerName[]>(url, this.options()).pipe(
            catchError(
                error => this.onError.alert("Could not fetch customer names", error)))
    }

    changeCustomerPassword(customerId: string, newPassword: string): Observable<{}> {
        const url = `${serverUrl}api/customer/changePassword/${customerId}/${newPassword}`
        return this.http.put(url, null, {...this.options(), responseType: 'text'}).pipe(
            catchError(error => this.onError.alert("Could not change password", error)))
    }

    loginCustomer(emailAddress: string,
        password: string): Observable<LoginCustomerResult> {
        const url = `${serverUrl}api/customer/byCredentials/${emailAddress}/${password}`
        return this.http.get<LoginCustomerResult>(url).pipe(
            catchError(error => this.onError.alert("Could not log in", error)))
    }

    notifyPasswordForgotten(emailAddress: string): Observable<{}> {
        const url = `${serverUrl}api/customer/forgotPassword/${emailAddress}`
        return this.http.get(url, {responseType: 'text'}).pipe(
            catchError(error =>
                this.onError.alert("Could not notify server of forgotten password",
                    error)))
    }

    validateSubstituteCode(code: string, emailAddress: string):
        Observable<ValidateSubstituteCodeResult> {
        const url =
            `${serverUrl}api/customer/validateSubstituteCode/${emailAddress}/${code}`
        return this.http.get<ValidateSubstituteCodeResult>(url)
    }

    addCustomer(customer: Customer): Observable<{}> {
        const url = `${serverUrl}api/customer`
        return this.http.post(url, customer, {...this.options(), responseType: 'text'})
            .pipe(
                catchError(error => this.onError.alert("Could not add customer", error)))
    }

    updateCustomer(customer: Customer): Observable<{}> {
        const url = `${serverUrl}api/customer`
        return this.http
            .put(url, customer, {...this.options(), responseType: 'text'}).pipe(
                catchError(
                    error => this.onError.alert("Could not update customer", error)))
    }

    validateEmailAddress(emailAddress: string): Observable<string> {
        const url = `${serverUrl}api/customer/validateAddress/${emailAddress}`
        return this.http.get(url, {responseType: 'text'}).pipe(
            catchError(
                error => this.onError.alert("Could not validate e-mail address", error)))
    }

    createCustomer(
        firstName: string, lastName: string, emailAddress: string, password: string):
        Observable<{}> {
        const url =
            `${serverUrl}api/customer/${firstName}/${lastName}/${emailAddress}/${password}`
        return this.http.post(url, null, {responseType: 'text'}).pipe(
            catchError(error => this.onError.alert("Could not create customer", error)))
    }
}

export interface CustomersPageResponse {
    customers: Customer[];
    count: number;
}

export interface CustomersPageResult {
    customers: Customer[];
    totalCount: number;
}

export interface LoginCustomerResult {
    customer: Customer;
    token: string;
    errorMessage: string;
}

export interface ValidateSubstituteCodeResult {
    errorMessage: string;
    customer: Customer;
    token: string;
}

export interface CustomerName {
    _id: string;
    firstName: string;
    lastName: string;
}