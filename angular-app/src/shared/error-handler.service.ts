import {Injectable} from "@angular/core"
import {ConfirmModalComponent} from "shared/controls/confirm-modal.component"
import {BsModalService} from "ngx-bootstrap/modal"
import {Observable} from "rxjs"
import {throwError} from "rxjs/internal/observable/throwError"

@Injectable()
export class ErrorHandler {
    constructor(private modalService: BsModalService) {}

    alert(message: string, error: any): Observable<any> {
        console.error(message, error)
        const initialState = {
            title: message,
            text: error.error || error
        }
        this.modalService.show(ConfirmModalComponent, {initialState})
        return throwError(error)
    }
}
