import {Component, Input} from "@angular/core"
import {Observable} from "rxjs"

@Component({
    selector: 'dv-change-password-form',
    templateUrl: './change-password-form.component.html'
})
export class ChangePasswordFormComponent {
    newPassword: string

    reenteredPassword: string

    isPasswordChanged = false

    @Input() makeChange: (password: string) => Observable<{}>

    areEntriesValid(): boolean {
        const minLength = 5
        return this.newPassword && this.reenteredPassword
            && this.newPassword.length >= minLength
            && this.reenteredPassword.length >= minLength
            && this.reenteredPassword == this.newPassword
    }

    changePassword() {
        this.makeChange(this.newPassword)
            .subscribe(ignore => this.isPasswordChanged = true)
    }
}
