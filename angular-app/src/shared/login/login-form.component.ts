import {Component, EventEmitter, Input, Output} from '@angular/core'
import {Observable} from "rxjs"

@Component({
    selector: 'dv-login-form',
    templateUrl: './login-form.component.html'
})
export class LoginFormComponent {
    @Input() userTypeName: string
    @Input() emailAddress = ""
    @Output() emailAddressChange = new EventEmitter<string>()
    password: string
    @Input() logIn: (emailAddress: string, password: string) => Observable<boolean>
    isLoginInvalid: boolean

    constructor() {}

    onLoginClick = () => {
        this.isLoginInvalid = false
        this.logIn(this.emailAddress, this.password)
            .subscribe(valid => this.isLoginInvalid = !valid)
    }

    shouldDisableLoginButton(): boolean {
        return !this.emailAddress || this.emailAddress.length == 0
            || !this.password || this.password.length == 0
    }
}

