import {User} from "shared/models/User"

export interface Admin extends User {
    _id: string
    shouldNotifyOfOrders: boolean
}
