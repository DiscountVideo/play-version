import {isEmailAddressValid, isPhoneNumberValid, isZipCodeValid} from "../Validators"
import {User} from "./User"

export class Customer implements User {
    _id: string
    firstName: string
    lastName: string
    address: string
    city: string
    state: string
    zipCode: string
    phoneNumber: string
    emailAddress: string
    credit: number
}

export function areCustomerFieldsValid(c: Customer): boolean {
    return c.firstName && c.firstName.length > 0
        && c.lastName && c.lastName.length > 0
        && c.address && c.address.length > 0
        && c.city && c.city.length > 0
        && c.state && c.state.length > 0
        && isZipCodeValid(c.zipCode)
        && isPhoneNumberValid(c.phoneNumber)
        && isEmailAddressValid(c.emailAddress)
}
