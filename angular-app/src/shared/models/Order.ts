import {Customer} from "./Customer"

export interface Order {
    _id: string
    date: Date
    customer: Customer
    videos: OrderVideo[]
    rentalsDueDate: Date
    productsTotal: number
    creditApplied: number
    tax: number
    shippingTotal: number
    finalPrice: number
    isShipped: boolean
}

export interface OrderVideo {
    _id: string
    code: string
    title: string
    isRental: boolean
    price: number
    isReturned: boolean
}

