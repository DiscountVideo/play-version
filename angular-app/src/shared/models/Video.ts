export interface Video {
    isRental: boolean
    _id?: string
    code?: string
    title?: string
    studio?: string
    director?: string
    year?: number
    tags: string[]
    status: number
    statusCustomerId?: string
    statusDate?: Date
    consignmentCustomerId?: string
    consignmentDate?: Date
    creditGiven: number
}