import {catchError, map} from "rxjs/operators"
import {Injectable} from "@angular/core"
import {HttpClient} from "@angular/common/http"
import {AuthService} from "../auth.service"
import {ErrorHandler} from "../error-handler.service"
import {Order, OrderVideo} from "../models/Order"
import {Observable} from "rxjs"
import {serverUrl} from "../Constants"

@Injectable()
export class OrdersService {
    constructor(
        private http: HttpClient, private auth: AuthService,
        private onError: ErrorHandler) {
    }

    private readonly options = () => ({headers: this.auth.httpHeaders})

    fetchOrder(id: String): Observable<Order> {
        const url = `${serverUrl}api/order/${id}`
        return this.http.get<Order>(url, this.options()).pipe(
            map(order => ({
                ...order,
                date: new Date(order.date),
                rentalsDueDate: (order.rentalsDueDate != null) ?
                    new Date(order.rentalsDueDate) : null
            })),
            catchError(
                error => this.onError.alert("Could not fetch order", error)))
    }

    fetchOrdersPage(
        pageNumber: number, firstLetter: string, ordersPerPage: number,
        customerNameSearchText: string, shouldExcludeShipped: boolean,
        shouldLimitToOverdue: boolean, shouldExcludeReturned: boolean)
        : Observable<OrdersPageResult> {
        const url = `${serverUrl}api/orders`
        const params: any = {
            pageNumber,
            ordersPerPage,
            shouldExcludeShipped,
            shouldLimitToOverdue,
            shouldExcludeReturned
        }
        if (firstLetter != "*") params.firstLetter = firstLetter
        if (!!customerNameSearchText) params.customerNameSearchText = customerNameSearchText
        return this.http.get<OrdersPageResponse>(url, {...this.options(), params}).pipe(
            map(response => ({
                orders: response.orders.map(o =>
                    ({...o, rentalsDueDate: new Date(o.rentalsDueDate)})),
                totalCount: response.count
            })),
            catchError(
                error => this.onError.alert("Could not fetch orders page",
                    error)))
    }

    fetchRentalOrdersForCustomer(customerId: string): Observable<RentalOrder[]> {
        const url = `${serverUrl}api/orders/rental/${customerId}`
        return this.http.get<RentalOrder[]>(url, this.options()).pipe(
            map(orders => orders.map(order => ({
                ...order,
                date: new Date(order.date),
                dueDate: new Date(order.dueDate)
            }))),
            catchError(
                error => this.onError.alert("Could not fetch rental orders for customers",
                    error)))
    }

    addOrder(order: Order): Observable<string> {
        const url = `${serverUrl}api/order`
        return this.http.post(url, order,
            {...this.options(), responseType: "text"}).pipe(
            catchError(
                error => this.onError.alert("Could not create order", error)))
    }

    updateIsShipped(order: Order): Observable<{}> {
        const url = `${serverUrl}api/order/isShipped/${order._id}/${order.isShipped}`
        return this.http.put(url, null,
            {...this.options(), responseType: "text"}).pipe(
            catchError(error =>
                this.onError.alert(
                    "Could not update is-shipped status of order", error)))
    }

    updateIsReturned(orderId: string, video: OrderVideo): Observable<{}> {
        const url =
            `${serverUrl}api/order/isReturned/${orderId}/${video._id}/${video.isReturned}`
        return this.http.put(url, null,
            {...this.options(), responseType: "text"}).pipe(
            catchError(error =>
                this.onError.alert("Could not update returned status of video",
                    error)))
    }
}

export interface OrdersPageResponse {
    orders: Order[];
    count: number;
}

export interface OrdersPageResult {
    orders: Order[];
    totalCount: number;
}

interface RentalOrder {
    date: Date
    videos: RentalOrderVideo[]
    dueDate: Date
    isShipped: boolean
}

interface RentalOrderVideo {
    title: string
    isReturned: boolean
}
