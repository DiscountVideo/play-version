import {NgModule} from '@angular/core'
import {ConfirmModalComponent} from "shared/controls/confirm-modal.component"
import {ErrorHandler} from "shared/error-handler.service"
import {BsModalService, ModalModule} from "ngx-bootstrap/modal"
import {ItemsPerPageSelectComponent} from "./controls/items-per-page-select.component"
import {SearchFieldComponent} from "./controls/search-field.component"
import {SearchSelectComponent} from "./controls/search-select.component"
import {ChangePasswordFormComponent} from "./login/change-password-form.component"
import {ConsignmentTableComponent} from "./customers/consignment-table.component"
import {LoginFormComponent} from "./login/login-form.component"
import {FirstLetterFilterComponent} from "./controls/first-letter-filter.component"
import {CommonModule} from "@angular/common"
import {FormsModule} from "@angular/forms"
import {HttpClientModule} from "@angular/common/http"
import {CustomerInfoFormComponent} from "./customers/customer-info-form.component"

@NgModule({
    declarations: [
        LoginFormComponent,
        FirstLetterFilterComponent,
        ItemsPerPageSelectComponent,
        SearchFieldComponent,
        SearchSelectComponent,
        CustomerInfoFormComponent,
        ChangePasswordFormComponent,
        ConsignmentTableComponent,
        ConfirmModalComponent
    ],
    exports: [
        LoginFormComponent,
        FirstLetterFilterComponent,
        ItemsPerPageSelectComponent,
        SearchFieldComponent,
        SearchSelectComponent,
        CustomerInfoFormComponent,
        ChangePasswordFormComponent,
        ConsignmentTableComponent,
        ConfirmModalComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ModalModule.forRoot()
    ],
    providers: [BsModalService, ErrorHandler],
    entryComponents: []
})
export class SharedModule {
}


