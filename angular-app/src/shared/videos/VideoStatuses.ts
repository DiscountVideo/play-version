export class VideoStatus {
    id: number
    display: string

    constructor(id: number, display: string) {
        this.id = id
        this.display = display
    }
}

export const videoStatuses = {
    available: new VideoStatus(0, "available"),
    inCart: new VideoStatus(1, "in cart"),
    rented: new VideoStatus(2, "rented"),
    sold: new VideoStatus(3, "sold")
}

