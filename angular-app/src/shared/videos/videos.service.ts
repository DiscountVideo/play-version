import {catchError, map} from 'rxjs/operators'
import {Video} from "../models/Video"
import {Injectable} from "@angular/core"
import {serverUrl} from "../Constants"
import {ErrorHandler} from "../error-handler.service"
import {AuthService} from "../auth.service"
import {Observable} from "rxjs"
import {HttpClient} from "@angular/common/http"

@Injectable()
export class VideosService {
    constructor(
        private http: HttpClient, private auth: AuthService, private onError: ErrorHandler) {}

    private readonly options = () => ({headers: this.auth.httpHeaders})

    fetchVideo(id: String): Observable<Video> {
        const url = `${serverUrl}api/video/${id}`
        return this.http.get<Video>(url).pipe(
            catchError(error => this.onError.alert("Could not fetch video", error)))
    }

    fetchVideosPage(
        isForRentals: boolean, pageNumber: number, firstLetter: string,
        videosPerPage: number, titleSearchText: string, searchTag: string = null,
        searchStudio: string = null, codeSearchText: string = null):
        Observable<VideosPageResult> {
        const url = `${serverUrl}api/videos`
        const params: any = {
            isForRentals,
            pageNumber,
            videosPerPage,
        }
        if (firstLetter != "*") params.firstLetter = firstLetter
        if (!!titleSearchText) params.titleSearchText = titleSearchText
        if (!!searchTag) params.searchTag = searchTag
        if (!!searchStudio) params.searchStudio = searchStudio
        if (!!codeSearchText) params.codeSearchText = codeSearchText
        return this.http.get<VideosPageResponse>(url, {params}).pipe(
            map(response => ({
                videos:
                    response.videos.map(v =>
                        ({...v, statusDate: new Date(v.statusDate)})),
                totalCount: response.count
            })),
            catchError(
                error => this.onError.alert("Could not fetch videos page", error)))
    }

    fetchAllTags(): Observable<string[]> {
        const url = `${serverUrl}api/tags/all`
        return this.http.get<string[]>(url).pipe(
            catchError(error => this.onError.alert("Could not fetch all tags", error)))
    }

    fetchTags(isForRenting: boolean): Observable<string[]> {
        const url = `${serverUrl}api/tags?isForRentals=${isForRenting}`
        return this.http.get<string[]>(url).pipe(
            catchError(error => this.onError.alert("Could not fetch tags", error)))
    }

    fetchAllStudios(): Observable<string[]> {
        const url = `${serverUrl}api/studios/all`
        return this.http.get<string[]>(url).pipe(
            catchError(error => this.onError.alert("Could not fetch all studios", error)))
    }

    fetchStudios(isForRenting: boolean): Observable<string[]> {
        const url = `${serverUrl}api/studios?isForRentals=${isForRenting}`
        return this.http.get<string[]>(url).pipe(
            catchError(error => this.onError.alert("Could not fetch studios", error)))
    }

    fetchConsignmentVideos(customerId: string): Observable<Video[]> {
        const url = `${serverUrl}api/videos/consignment/${customerId}`
        return this.http.get<Video[]>(url, this.options()).pipe(
            catchError(error =>
                this.onError.alert("Could not fetch consignment videos", error)))
    }

    addVideo(video: Video): Observable<{}> {
        const url = `${serverUrl}api/video`
        return this.http.post(url, video, {...this.options(), responseType: 'text'}).pipe(
            catchError(error => this.onError.alert("Could not add video", error)))
    }

    updateVideo(video: Video): Observable<{}> {
        const url = `${serverUrl}api/video`
        return this.http
            .put(url, video, {...this.options(), responseType: 'text'}).pipe(
                catchError(error => this.onError.alert("Could not update video", error)))

    }

    deleteVideo(id: string): Observable<{}> {
        const url = `${serverUrl}api/video/${id}`
        return this.http
            .delete(url, {...this.options(), responseType: 'text'}).pipe(
                catchError(error => this.onError.alert("Could not delete video", error)))
    }
}

export interface VideosPageResponse {
    videos: Video[];
    count: number;
}

export interface VideosPageResult {
    videos: Video[];
    totalCount: number;
}
