name := """DiscountVideo"""

version := "1.0"

lazy val scalaV = "2.13.6"
scalaVersion := scalaV

lazy val server =
    (project in file("server"))
        .dependsOn(storage)
        .enablePlugins(PlayScala)
        .settings(
            scalaVersion := scalaV,
            updateOptions := updateOptions.value.withCachedResolution(true),
            libraryDependencies ++= Seq(
                filters,
                "com.stripe" % "stripe-java" % "2.4.0",
                "com.github.daddykotex" %% "courier" % "2.0.0",
                "com.nimbusds" % "nimbus-jose-jwt" % "3.8.2"
            ),
            Assets / unmanagedResourceDirectories +=
                baseDirectory.value / "../angular-app/public"
        )

lazy val storage =
    (project in file("storage"))
        .settings(
            scalaVersion := scalaV,
            Compile / scalaSource := baseDirectory.value / "src",
            libraryDependencies ++= Seq(
                "com.typesafe.play" %% "play-json" % "2.9.2",
                "org.mongodb.scala" %% "mongo-scala-driver" % "4.0.1",
                "ch.qos.logback" % "logback-classic" % "1.1.8"
            )
        )

// load the server project at sbt startup
onLoad in Global :=
    (Command.process("project server", _: State)) compose (onLoad in Global).value

