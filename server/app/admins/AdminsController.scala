package admins

import app._
import auth.{JwtSigned, JwtUtil}
import org.mongodb.scala.bson.ObjectId
import play.api.libs.json.{Json, OFormat, OWrites, Reads, Writes}
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import storage.ObjectIdFormat
import storage.admins.Admin

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class AdminsController (components: ControllerComponents, jwtUtil: JwtUtil)
    extends AbstractController(components) {
    private val adminRepository = Repositories.createAdminRepository()

    private implicit val objectIdReads: Reads[ObjectId] = ObjectIdFormat.reads
    private implicit val objectIdWrites: Writes[ObjectId] = ObjectIdFormat.writes
    private implicit val adminFormat: OFormat[Admin] = Json.format[Admin]
    private implicit val fetchAdminByCredentialsWrites: OWrites[FetchAdminByCredentialsResult] = Json
        .writes[FetchAdminByCredentialsResult]

    def fetchAdmin(adminId: String): Action[AnyContent] = JwtSigned.async {
        adminRepository.fetchAdmin(adminId).map(admin => Ok(Json.toJson(admin)))
            .recover(ErrorHandler("Could not fetch admin"))
    }

    def fetchAdmins(): Action[AnyContent] = JwtSigned.async {
        adminRepository.fetchAdmins().map(admins => Ok(Json.toJson(admins)))
            .recover(ErrorHandler("Could not fetch admins"))
    }

    def fetchAdminByCredentials(emailAddress: String,
        password: String): Action[AnyContent] = Action.async {
        adminRepository.fetchAdminByEmailAddress(emailAddress)
            .map(validatePassword(_, emailAddress, password))
            .map(result => Ok(Json.toJson(result)))
            .recover(ErrorHandler("Could not fetch admin"))
    }

    case class FetchAdminByCredentialsResult(var admin: Option[Admin] = None,
        var token: Option[String] = None, var errorMessage: Option[String] = None)

    private def validatePassword(adminOpt: Option[Admin], emailAddress: String,
        password: String): FetchAdminByCredentialsResult = {
        // if no admin matching the credentials was found
        val result = FetchAdminByCredentialsResult()
        if (adminOpt.isEmpty) {
            // respond as such
            result.errorMessage = Some("Admin not found")
            return result
        }

        // if the given password does not hash to the same result as the actual password
        val admin = adminOpt.get
        if (!Password.isPasswordValid(password, admin.salt, admin.hash)) {
            // respond that the login is invalid
            result.errorMessage = Some("Invalid log-in")
            return result
        }

        // respond with the admin found, after first removing
        // its password values; also, include a JWT for subsequent
        // API calls to supply in order to authenticate the admin
        admin.hash = ""
        admin.salt = ""
        result.admin = Some(admin)
        result.token = Some(jwtUtil.sign(emailAddress.toLowerCase()))
        result
    }

    def createAdmin(emailAddress: String,
        password: String): Action[AnyContent] = JwtSigned.async {
        // if we can't find an admin already in the database
        // with the given e-mail address
        val lowerAddress = emailAddress.toLowerCase()
        adminRepository.fetchAdminByEmailAddress(lowerAddress)
            .flatMap { case _: Some[Admin] => Future(BadRequest("address in use"))
            case None => // create a new admin in the database with the given
                // e-mail address and a salted hash of the given password
                val salt = Password.getSalt
                val hash = Password.generateStrongPasswordHash(password, salt)
                val admin = Admin(_id = new ObjectId(), emailAddress = lowerAddress,
                    hash = hash, salt = Password.convertBytesToHexString(salt),
                    shouldNotifyOfOrders = true)
                adminRepository.addAdmin(admin).map(_ => Ok("Admin created"))
            }.recover(ErrorHandler("Could not create admin"))
    }

    def updateShouldNotify(adminId: String,
        shouldNotify: Boolean): Action[AnyContent] = JwtSigned.async {
        adminRepository.updateShouldNotify(adminId, shouldNotify)
            .map(_ => Ok("Should-notify value updated"))
            .recover(ErrorHandler("Could not update should-notify value"))
    }

    def deleteAdmin(adminId: String): Action[AnyContent] = JwtSigned.async {
        adminRepository.deleteAdmin(adminId).map(_ => Ok("Admin deleted"))
            .recover(ErrorHandler("Could not delete admin"))
    }

    /**
     * Changes the password of the admin with the given ID in the database to that given.
     */
    def changePassword(adminId: String,
        newPassword: String): Action[AnyContent] = JwtSigned.async {
        // update the admin with a new salted hash of the given password
        val salt = Password.getSalt
        val hash = Password.generateStrongPasswordHash(newPassword, salt)
        adminRepository
            .updateAdminHash(adminId, hash, Password.convertBytesToHexString(salt))
            .map(_ => Ok("Password changed"))
            .recover(ErrorHandler("Could not change password"))
    }
}
