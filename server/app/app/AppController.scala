package app

import akka.actor.ActorSystem
import controllers.Assets
import play.api.Environment
import play.api.mvc._
import videos.CartChecker

import scala.concurrent.duration.Duration
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class AppController (components: ControllerComponents, system: ActorSystem, assets: Assets)(implicit environment: Environment)
    extends AbstractController(components) {
    /**
     * Redirects to the customer app.
     */
    def index(): Action[AnyContent] = Action {
        // redirect to the customer app
        Redirect("/app")
    }

    /**
     * Responds to all app requests with the Angular app, which will process the request
     * with its own internal routing (which is why the any parameter is ignored here).
     */
    def app(any: String): Action[AnyContent] =
        assets.at(path="/public/dist", file="index.html")

    // schedule the checking for videos which have been in carts too long to occur
    // every 15 minutes
    system.scheduler.scheduleAtFixedRate(Duration.Zero, 15.minutes)(() => CartChecker.removeVideosLeftInCarts())
}
