package app

import admins.AdminsController
import auth.{JwtSigned, JwtUtil}
import checkout.CheckoutController
import controllers.AssetsComponents
import customers.CustomersController
import orders.OrdersController
import play.api.ApplicationLoader.Context
import play.api.{Application, ApplicationLoader, BuiltInComponentsFromContext}
import play.filters.cors.CORSConfig.Origins
import play.filters.cors.{CORSConfig, CORSFilter}
import router.Routes
import storage.mongo.MongoRepository
import videos.VideosController

class AppLoader extends ApplicationLoader
{
    def load(context: Context): Application = new MyComponents(context).application
}

class MyComponents(context: Context) extends BuiltInComponentsFromContext(context)
    with AssetsComponents
{
    private val config = context.initialConfiguration
    val jwtUtil = new JwtUtil(config)
    lazy val appController = new AppController(controllerComponents, actorSystem, assets)(environment)
    lazy val videosController = new VideosController(controllerComponents)
    lazy val customersController = new CustomersController(controllerComponents, jwtUtil)
    lazy val checkoutController = new CheckoutController(controllerComponents)
    lazy val ordersController = new OrdersController(controllerComponents)
    lazy val adminsController = new AdminsController(controllerComponents, jwtUtil)

    MyComponents.components = this

    /**
     * Note that the order of the parameters corresponds to the order of their first
     * appearance in the routes file.
     */
    lazy val router =
        new Routes(
            httpErrorHandler, appController, videosController,
            customersController, checkoutController, ordersController, adminsController, assets)

    // add a CORS filter so that our Angular CLI client app can call our server API,
    // since the CLI app is served by the CLI server at port 4200, and needs to
    // call our server API on port 9000;
    // note that we have to set the CORS filter config to allow all origins,
    // even though the Play docs say it should be the default setting
    val corsConfig =
        new CORSConfig(
            allowedOrigins =
                Origins.Matching((origin: String) => origin == "http://localhost:4200")
        )
    val corsFilter = new CORSFilter(corsConfig, pathPrefixes = Seq("/"))

    override lazy val httpFilters = Seq(/*new HttpsRedirectFilter(), */corsFilter)

    JwtSigned.jwtUtil = jwtUtil

    Repositories.database =
        if (config.get[Boolean]("isForTesting"))
            MongoRepository.testDatabase
        else MongoRepository.productionDatabase
}

object MyComponents {
    /**
     * This reference (which is set, above) restores static access to our
     * application and environment objects, that was deprecated in Play 2.5 (which instead
     * expects these objects to be dependency-injected - see
     * https://www.playframework.com/documentation/2.5.x/Migration25#Deprecated-play.Play-and-play.api.Play-methods)
     */
    var components: MyComponents = _
}