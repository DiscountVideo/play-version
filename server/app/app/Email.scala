package app

import com.typesafe.config.{ConfigFactory, ConfigParseOptions}

import javax.mail.internet.InternetAddress
import courier.{Envelope, Mailer, Text}
import play.api.Logger

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Email
{
    private val logger = Logger("Email")

    def send(to: String, subject: String, text: String): Future[Unit] =
    {
        // note that the application class-loader is used here, instead of the
        // default context class-loader, as the latter cannot see our custom email
        // config file, whereas the former can
        // (see https://www.playframework.com/documentation/2.8.x/ThreadPools#Application-class-loader)
        val options = ConfigParseOptions.defaults().setAllowMissing(false)
            .setClassLoader(MyComponents.components.environment.classLoader)
        val config = ConfigFactory.parseResources("email.conf", options)
        val host = config.getString("host")
        val port = config.getInt("port")
        val login = config.getString("login")
        val password = config.getString("password")
        val mailer =
            Mailer(host, port)
                .sslSocketFactory
                .auth(true)
                .as(login, password)
                .startTls(true)()
        val envelope =
            Envelope
                .from(new InternetAddress("Discount Video <DiscountVideo@aol.com>"))
                .to(new InternetAddress(to))
                .subject(subject)
                .content(Text(text))
        mailer(envelope)
            .map(_ => logger.info(s"E-mail sent to $to"))
            .recover(t => logger.error(s"Could not send e-mail to $to", t))
    }
}
