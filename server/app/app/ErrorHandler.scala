package app

import play.api.Logger
import play.api.libs.json.JsError
import play.api.mvc.Result
import play.api.mvc.Results._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.control.NonFatal

object ErrorHandler
{
    private val logger = Logger("ErrorHandler")

    def apply(message: String): PartialFunction[Throwable, Result] =
    {
        case NonFatal(t) =>
            logger.error(message, t)
            InternalServerError(message + ": " + t.getMessage)
    }
}

object JsErrorHandler
{
    private val logger = Logger("JsErrorHandler")

    def apply(message: String, error: JsError): Future[Result] =
    {
        val errorJson = JsError.toJson(error)
        logger.error(message + ": " + errorJson)
        Future(InternalServerError(message + ": " + errorJson))
    }
}
