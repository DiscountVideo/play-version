package app

import java.math.BigInteger
import java.security.SecureRandom
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

object Password
{
    def isPasswordValid(
        password: String, salt: String, storedHashString: String): Boolean =
    {
        // convert the salt to the form we used when generating password hashes
        // with the Node version of this app; otherwise, logins will work for
        // those hashes or the ones generated here, but not both
        val nodeStyleSalt = salt.getBytes

        val storedHash = convertHexStringToBytes(storedHashString)
        val spec =
            new PBEKeySpec(
                password.toCharArray, nodeStyleSalt, 1000, storedHash.length * 8)
        val skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
        val computedHash = skf.generateSecret(spec).getEncoded

        var diff = storedHash.length ^ computedHash.length
        for (i: Int <- storedHash.indices) diff |= storedHash(i) ^ computedHash(i)
        diff == 0
    }

    def convertHexStringToBytes(hex: String): Array[Byte] =
    {
        val bytes = new Array[Byte](hex.length / 2)
        for (i: Int <- bytes.indices)
            bytes(i) = Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16).toByte
        bytes
    }

    def generateStrongPasswordHash(password: String, salt: Array[Byte]): String =
    {
        // convert the salt to the form we used when generating password hashes
        // with the Node version of this app; otherwise, logins will work for
        // those hashes or the ones generated here, but not both
        val nodeStyleSalt = convertBytesToHexString(salt).getBytes

        val spec = new PBEKeySpec(password.toCharArray, nodeStyleSalt, 1000, 64 * 8)
        val skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
        val hash = skf.generateSecret(spec).getEncoded
        convertBytesToHexString(hash)
    }

    def convertBytesToHexString(bytes: Array[Byte]): String =
    {
        val bi = new BigInteger(1, bytes)
        val hex = bi.toString(16)
        val paddingLength = (bytes.length * 2) - hex.length()
        if (paddingLength > 0) String.format("%0" + paddingLength + "d", Int.box(0)) + hex
        else hex
    }

    def getSalt: Array[Byte] =
    {
        val sr = SecureRandom.getInstance("SHA1PRNG")
        val salt = new Array[Byte](16)
        sr.nextBytes(salt)
        salt
    }
}
