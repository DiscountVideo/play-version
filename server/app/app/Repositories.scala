package app

import org.mongodb.scala.MongoDatabase
import storage.admins.{AdminRepository, MongoAdminRepository}
import storage.customers.{CustomerRepository, MongoCustomerRepository}
import storage.orders.{MongoOrderRepository, OrderRepository}
import storage.videos.{MongoVideoRepository, VideoRepository}

/**
 * Provides database-agnostic references to database-specific repositories.
 */
object Repositories {
    var database: MongoDatabase = _

    def createVideoRepository(): VideoRepository = new MongoVideoRepository(database)

    def createCustomerRepository(): CustomerRepository = new MongoCustomerRepository(
        database)

    def createOrderRepository(): OrderRepository = new MongoOrderRepository(database)

    def createAdminRepository(): AdminRepository = new MongoAdminRepository(database)
}
