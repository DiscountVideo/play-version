package app

import javax.print.{DocFlavor, PrintServiceLookup, SimpleDoc}

object Util
{
    def asMoney(amount: Float, shouldJustify: Boolean = true) =
    {
        var justify = "  "
        val absAmount = Math.abs(amount)
        if (absAmount < 10) justify = "   "
        else if (absAmount >= 100) justify = " "
        (if (shouldJustify) justify else "") + (if (amount < 0) "-" else "") +
            "$" + absAmount.formatted("%.2f")
    }

    def printText(text: String) =
    {
        val job = PrintServiceLookup.lookupDefaultPrintService().createPrintJob()
        val doc = new SimpleDoc(text, DocFlavor.STRING.TEXT_PLAIN, null)
        job.print(doc, null)
    }

    /**
     * Returns a random integer from low to high, inclusive.
     */
    def randomInt(low: Int, high: Int): Int =
    {
        Math.floor(Math.random() * (high - low) + low).toInt
    }
}
