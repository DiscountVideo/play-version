package auth

import play.api.http.HeaderNames._
import play.api.mvc.Results._
import play.api.mvc.{ActionBuilder, BodyParser, BodyParsers, Request, Result, WrappedRequest}

import scala.concurrent.{ExecutionContext, Future}

class JwtSignedRequest[A](val jwt: String, request: Request[A])
    extends WrappedRequest[A](request)

object JwtSigned extends ActionBuilder[JwtSignedRequest, Any]
{
    var jwtUtil: JwtUtil = _

    def parser: BodyParser[Any] = BodyParsers.utils.empty

    def invokeBlock[A](req: Request[A], block: JwtSignedRequest[A] => Future[Result]): Future[Result] =
    {
        req.headers.get(AUTHORIZATION) map {
            token =>
                if (jwtUtil.verify(token)) block(new JwtSignedRequest(token, req))
                else Future.successful(Forbidden)
        } getOrElse Future.successful(Unauthorized)
    }

    override protected def executionContext: ExecutionContext = ExecutionContext.global
}