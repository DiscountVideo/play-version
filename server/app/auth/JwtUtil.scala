package auth

import java.util.Date
import com.nimbusds.jose.crypto.{MACSigner, MACVerifier}
import com.nimbusds.jose.{JWSAlgorithm, JWSHeader}
import com.nimbusds.jwt.{JWTClaimsSet, ReadOnlyJWTClaimsSet, SignedJWT}
import play.api._

class JwtUtil(configuration: Configuration) {
    private val logger = Logger(classOf[JwtUtil])

    val tokenPrefix = "Bearer "

    private val issuer = configuration.get[String]("jwt.issuer")
    if (issuer == null) throw new IllegalStateException("JWT issuer is missing from config file")

    private val sharedSecret = configuration.get[String]("jwt.sharedSecret")
    if (sharedSecret == null) throw new IllegalStateException("JWT shared secret is missing from config file")

    private val expiryTime = configuration.get[Int]("jwt.expiryInSecs")
    if (expiryTime == 0) throw new IllegalStateException("JWT expiry is missing from config file")

    private val algorithm = new JWSHeader(JWSAlgorithm.HS256)

    private lazy val signer: MACSigner = new MACSigner(sharedSecret)
    private lazy val verifier: MACVerifier = new MACVerifier(sharedSecret)

    signer
    verifier

    def verify(token: String): Boolean = {
        val payload = decode(token)

        // check expiration date
        if (!new Date().before(payload.getExpirationTime)) {
            logger.error("Token expired: " + payload.getExpirationTime)
            return false
        }

        // Match Issuer
        if (!payload.getIssuer.equals(issuer)) {
            logger.error("Issuer mismatch: " + payload.getIssuer)
            return false
        }
        true
    }

    def decode(token: String): ReadOnlyJWTClaimsSet = {
        val signedJWT = SignedJWT.parse(token.substring(tokenPrefix.length))
        if (!signedJWT.verify(verifier)) {
            throw new IllegalArgumentException("Json Web Token cannot be verified!")
        }
        signedJWT.getJWTClaimsSet
    }

    def sign(userInfo: String): String = {
        val claimsSet = new JWTClaimsSet()
        claimsSet.setSubject(userInfo)
        claimsSet.setIssueTime(new Date)
        claimsSet.setIssuer(issuer)
        claimsSet.setExpirationTime(new Date(claimsSet.getIssueTime.getTime + (expiryTime * 1000)))
        val signedJWT = new SignedJWT(algorithm, claimsSet)
        signedJWT.sign(signer)
        signedJWT.serialize()
    }
}
