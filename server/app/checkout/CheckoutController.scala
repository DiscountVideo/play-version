package checkout

import auth.JwtSigned
import com.stripe.model.{Charge, Customer => StripeCustomer}
import com.stripe.net.RequestOptions.RequestOptionsBuilder
import play.api.libs.json.JsValue
import play.api.mvc.{AbstractController, Action, ControllerComponents}

import scala.jdk.CollectionConverters._

class CheckoutController (components: ControllerComponents) extends AbstractController(components)
{
    def makeCharge(emailAddress: String, amount: Int): Action[JsValue] = JwtSigned(parse.json)
    {
        request =>
            // if we can create a new customer entity within the Stripe system
            val paymentTokenId = (request.body \ "id").as[String]
            val customerParams = MapHasAsJava[String, Object](Map(
                "source" -> paymentTokenId, "description" -> emailAddress)).asJava
            val requestOptions =
                new RequestOptionsBuilder()
                    .setApiKey("sk_test_UyYrhKk5G7Vq0FuSc7zQkiAH")
                    .build()
            val customer = StripeCustomer.create(customerParams, requestOptions)

            // create and issue a charge to that Stripe customer for the given amount
            val chargeParams = MapHasAsJava[String, Object](Map(
                "amount" -> Int.box(amount),
                "currency" -> "usd",
                "customer" -> customer.getId)).asJava
            Charge.create(chargeParams, requestOptions)
            Ok
    }
}
