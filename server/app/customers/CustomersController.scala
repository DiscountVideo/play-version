package customers

import java.util.Date
import app._
import auth.{JwtSigned, JwtUtil}
import org.mongodb.scala.bson.ObjectId
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json, OFormat, OWrites, Reads, Writes}
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import storage.ObjectIdFormat
import storage.customers.{Customer, CustomerPasswordHash}

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class CustomersController(components: ControllerComponents, jwtUtil: JwtUtil) extends AbstractController(components) {
    private val customerRepository = Repositories.createCustomerRepository()

    private implicit val objectIdReads: Reads[ObjectId] = ObjectIdFormat.reads
    private implicit val objectIdWrites: Writes[ObjectId] = ObjectIdFormat.writes
    private implicit val customerFormat: OFormat[Customer] = Json.format[Customer]
    private implicit val fetchCustomerByCredentialsWrites: OWrites[FetchCustomerByCredentialsResult] = Json
        .writes[FetchCustomerByCredentialsResult]

    /**
     * Maps a customer's e-mail address to a substitute login code generated
     * (and stored here for that customer), so we can later compare the code
     * submitted by a supposed customer to the that generated for it.
     */
    private val substituteCodes = new mutable.HashMap[String, SubstituteCode]

    def fetchCustomer(id: String): Action[AnyContent] = JwtSigned.async {
        customerRepository.fetchCustomer(id)
            .map(customerOpt => Ok(Json.toJson(customerOpt)))
            .recover(ErrorHandler("Could not fetch customer"))
    }

    def fetchCustomers(pageNumber: Int, customersPerPage: Int,
        firstLetter: Option[String],
        lastNameSearchText: Option[String]): Action[AnyContent] = JwtSigned.async {
        customerRepository
            .fetchCustomers(pageNumber, customersPerPage, firstLetter, lastNameSearchText)
            .map(fetch => Ok(
                Json.obj("customers" -> Json.toJson(fetch._1), "count" -> fetch._2)))
            .recover(ErrorHandler("Could not fetch customers"))
    }

    def addCustomer(): Action[JsValue] = JwtSigned.async(parse.json) { request =>
        request.body.validate[Customer] match {
            case JsSuccess(customer, _) => {
                // generate an e-mail address for the customer using the current time
                // (to make it unique)
                val time = new Date().getTime
                val emailAddress = "customer" + time + "@no.email"
                customer.emailAddress = Some(emailAddress)

                // add the customer to the repository
                customerRepository.addCustomer(customer).flatMap(_ => {
                    // set the customer's initial password to be its
                    // e-mail address
                    val salt = Password.getSalt
                    val password = CustomerPasswordHash(_id = customer._id,
                        salt = Some(Password.convertBytesToHexString(salt)), hash = Some(
                            Password.generateStrongPasswordHash(emailAddress, salt)))
                    customerRepository.addPasswordHash(password).map(_ => Ok)
                }).recover(ErrorHandler("Could not add customer"))
            }
            case JsError(errors) => Future(BadRequest(JsError.toJson(errors)))
        }
    }

    def updateCustomer(): Action[JsValue] = JwtSigned.async(parse.json) { request =>
        request.body.validate[Customer] match {
            case JsSuccess(customer, _) => customerRepository.updateCustomer(customer)
                .map(_ => Ok).recover(ErrorHandler("Could not update customer"))
            case JsError(_) => Future(BadRequest)
        }
    }

    def fetchCustomerByCredentials(emailAddress: String,
        password: String): Action[AnyContent] = Action.async {
        customerRepository.fetchCustomerByEmailAddress(emailAddress)
            .flatMap(customerOpt => {
                // if no customer matching the credentials was found
                if (customerOpt.isEmpty) {
                    // respond as such
                    val result = FetchCustomerByCredentialsResult()
                    result.errorMessage = Some("Customer not found")
                    Future(result)
                } else validatePassword(customerOpt.get, emailAddress, password)
            }).map(result => Ok(Json.toJson(result)))
            .recover(ErrorHandler("Could not fetch customer"))
    }

    case class FetchCustomerByCredentialsResult(var customer: Option[Customer] = None,
        var token: Option[String] = None, var errorMessage: Option[String] = None)

    private def validatePassword(customer: Customer, emailAddress: String,
        password: String): Future[FetchCustomerByCredentialsResult] = {
        val result = FetchCustomerByCredentialsResult()
        customerRepository.fetchPasswordHash(customer._id.toHexString)
            .map(passwordHashOpt => {
                // if no password for the given customer's ID was found
                if (passwordHashOpt.isEmpty) {
                    // respond as such
                    result.errorMessage = Some("Password not found")
                } else {
                    // if the given password does not hash to the same result
                    // as the actual password
                    val passwordHash = passwordHashOpt.get
                    if (!Password.isPasswordValid(password, passwordHash.salt.get,
                        passwordHash.hash.get)) {
                        // respond that the login is invalid
                        result.errorMessage = Some("Invalid log-in")
                    } else {
                        // respond with the customer found; also, include a JWT for
                        // subsequent API calls to supply in order to authenticate
                        // the customer
                        result.customer = Some(customer)
                        result.token = Some(jwtUtil.sign(emailAddress.toLowerCase()))
                    }
                }

                result
            })
    }

    /**
     * Checks whether there is a customer in the database with the given e-mail address.
     * If there isn't, this sends an e-mail containing a validation code to that address.
     * The customer will have to submit that code to the caller to validate their
     * e-mail address.
     */
    def validateAddress(emailAddress: String): Action[AnyContent] = Action.async {
        // if we can't find a customer in the database with the given e-mail address
        val address = emailAddress.toLowerCase()
        customerRepository.fetchCustomerByEmailAddress(address).map
        { case _: Some[Customer] => BadRequest("E-mail address already has account")
        case None => // generate a validation code randomly, and include it in an
            // e-mail to the given address
            val code = Util.randomInt(1000, 10000)
            val text = "Here is your validation code so you may create a new" +
                " Discount Video account: " + code +
                "\n\nPlease enter this code in the Create Account section" +
                " of the Log In page." + "\n\nThank you,\nDiscount Video" +
                "\nhttp://gaydiscountvideo.com"
            Email.send(address, "Validation code for new Discount Video account", text)

            // respond with the validation code so the caller can match
            // it against the code entered by the user (the check could
            // be performed here in the API, instead, to be more secure,
            // but it's not a big vulnerability)
            Ok(code.toString)
        }.recover(
            ErrorHandler("Could not check for existing customer with given address"))
    }

    /**
     * Creates a new customer in the database, with the given name, email address,
     * and password.
     */
    def createCustomer(firstName: String, lastName: String, emailAddress: String,
        password: String): Action[AnyContent] = Action.async {
        val customer = Customer(_id = new ObjectId(), firstName = firstName,
            lastName = lastName, emailAddress = Some(emailAddress))
        customerRepository.addCustomer(customer).flatMap(_ => {
            val salt = Password.getSalt
            val saltString = Password.convertBytesToHexString(salt)
            val hash = Password.generateStrongPasswordHash(password, salt)
            val passwordHash = CustomerPasswordHash(customer._id, Some(hash),
                Some(saltString))
            customerRepository.addPasswordHash(passwordHash)
                .map(_ => Ok(Json.toJson(customer)))
        }).recover(ErrorHandler("Could not add customer to repository"))
    }

    /**
     * Generates a random substitute login code and sends it in an e-mail to the
     * given address.
     */
    def sendPasswordSubstitute(emailAddress: String): Action[AnyContent] = Action.async {
        // if we can find a customer in the database with the given e-mail address
        val address = emailAddress.toLowerCase()
        customerRepository.fetchCustomerByEmailAddress(address)
            .map { case None => NotFound("Customer not found")
            case _: Some[Customer] => sendPasswordSubstituteToCustomer(address)
                Ok("Password substitute message sent")
            }.recover(ErrorHandler("Could not send password substitute"))
    }

    case class SubstituteCode(code: Int, // the code value itself
        date: Date // the date/time the code value was generated (since it will expire)
    )

    private def sendPasswordSubstituteToCustomer(emailAddress: String): Unit = {
        // generate a random substitute login code and store it externally
        // to this method, so we may verify against it when called to do so
        val code = Util.randomInt(100000, 1000000)
        substituteCodes.put(emailAddress, SubstituteCode(code, new Date()))

        // send the substitute login code in an e-mail to the given address
        val text = "Here is your substitute login code: " + code +
            "\n\nThank you,\nDiscount Video" + "\nhttp://gaydiscountvideo.com"
        Email.send(emailAddress, "Your substitute login code from Discount Video", text)
    }

    /**
     * Compares the given substitute login code with that stored for the given e-mail
     * address.  If they are the same, and the code hasn't expired, then this responds
     * with the customer from the database with the given e-mail address.
     */
    def validateSubstituteCode(emailAddress: String,
        code: Int): Action[AnyContent] = Action.async {
        // if the substitute login code supplied by the request does not match that
        // stored for the given e-mail address
        val address = emailAddress.toLowerCase()
        val storedCodeOpt = substituteCodes.get(address)
        if (storedCodeOpt.isEmpty ||
            code != storedCodeOpt.get.code) // respond that the code is invalid
        Future {
            NotFound("Invalid code")
        }

        // else if the substitute login code has expired
        else if ( {
            val timeDiff = new Date().getTime - storedCodeOpt.get.date.getTime
            val oneDayInMs = 1000 * 60 * 60 * 24
            timeDiff > oneDayInMs
        }) // respond that it has expired
        Future {
            NotFound("Code has expired")
        }

        // otherwise, find the customer in the database with the given e-mail address
        else customerRepository.fetchCustomerByEmailAddress(address).map(customerOpt => {
            // if the customer is not found, respond as such
            if (customerOpt.isEmpty) NotFound(
                "Could not find customer with matching e-mail address")

            // otherwise
            else {
                // respond with the customer and a JWT generated from the
                // given e-mail address
                val reply = Json
                    .obj("customer" -> customerOpt.get, "token" -> jwtUtil.sign(address))
                Ok(reply)
            }
        }).recover(ErrorHandler("Could not find customer with matching e-mail address"))
    }

    /**
     * Changes the password of the customer with the given ID in the database to that given.
     */
    def changePassword(customerId: String,
        newPassword: String): Action[AnyContent] = JwtSigned.async {
        // update the customer with a new salted hash of the given password
        val salt = Password.getSalt
        val hash = Password.generateStrongPasswordHash(newPassword, salt)
        customerRepository
            .updatePasswordHash(customerId, hash, Password.convertBytesToHexString(salt))
            .map(_ => Ok("Password changed"))
            .recover(ErrorHandler("Could not change password"))
    }

    /**
     * Fetches all the customers in the repository, with only their name and ID
     * fields populated.
     */
    def fetchNames(): Action[AnyContent] = JwtSigned.async {
        customerRepository.fetchNames().map(names => Ok(Json.toJson(names)))
            .recover(ErrorHandler("Could not fetch customer names"))
    }
}