package filters

import akka.stream.Materializer
import play.api.mvc.{Filter, RequestHeader, Result, Results}

import scala.concurrent.Future

class HttpsRedirectFilter (implicit val mat: Materializer)
    extends Filter
{
    def apply(nextFilter: (RequestHeader) => Future[Result])
        (requestHeader: RequestHeader): Future[Result] =
    {
        val sslPort = 443
        if (!requestHeader.secure)
            Future.successful(
                Results.Redirect(
                    s"https://${requestHeader.domain}:$sslPort${requestHeader.uri}"))
        else nextFilter(requestHeader)
    }
}
