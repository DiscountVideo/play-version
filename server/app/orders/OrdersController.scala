package orders

import java.text.DateFormat
import java.util.Date
import app.{ErrorHandler, _}
import auth.JwtSigned
import org.mongodb.scala.bson.ObjectId
import play.api.libs.json._
import play.api.mvc._
import storage.{JsonDate, ObjectIdFormat}
import storage.customers.Customer
import storage.orders.{Order, OrderVideo}
import storage.videos.{Video, VideoStatuses}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.implicitConversions

class OrdersController (components: ControllerComponents) extends AbstractController(components) {
    private val orderRepository = Repositories.createOrderRepository()
    private val customerRepository = Repositories.createCustomerRepository()
    private val videoRepository = Repositories.createVideoRepository()
    private val adminRepository = Repositories.createAdminRepository()

    private implicit val objectIdReads: Reads[ObjectId] = ObjectIdFormat.reads
    private implicit val objectIdWrites: Writes[ObjectId] = ObjectIdFormat.writes
    private implicit val dateReads: Reads[Date] = JsonDate.dateReads
    private implicit val dateWrites: Writes[Date] = JsonDate.dateWrites
    private implicit val customerFormat: OFormat[Customer] = Json.format[Customer]
    private implicit val orderVideoFormat: OFormat[OrderVideo] = Json.format[OrderVideo]
    private implicit val orderFormat: OFormat[Order] = Json.format[Order]
    private implicit val customerRentalOrderVideoWrites: Writes[RentalOrderVideo] = Json.writes[RentalOrderVideo]
    private implicit val customerRentalOrderWrites: Writes[RentalOrder] = Json.writes[RentalOrder]

    private val shouldPrint = false

    def fetchOrder(id: String): Action[AnyContent] = JwtSigned.async {
        orderRepository.fetchOrder(id).map(order => Ok(Json.toJson(order.get)))
            .recover(ErrorHandler("Could not fetch order"))
    }

    def fetchOrders(pageNumber: Int, ordersPerPage: Int, firstLetter: Option[String],
        customerNameSearchText: Option[String], shouldExcludeShipped: Boolean,
        shouldLimitToOverdue: Boolean,
        shouldExcludeReturned: Boolean): Action[AnyContent] = JwtSigned.async {
        orderRepository
            .fetchOrders(pageNumber, ordersPerPage, firstLetter, customerNameSearchText,
                shouldExcludeShipped, shouldLimitToOverdue, shouldExcludeReturned)
            .map(fetch => {
                Ok(Json.obj("orders" -> Json.toJson(fetch._1), "count" -> fetch._2))
            }).recover(ErrorHandler("Could not fetch orders"))
    }

    def fetchRentalOrdersForCustomer(customerId: String): Action[AnyContent] = JwtSigned
        .async {
            orderRepository.fetchRentalOrdersForCustomer(customerId).map(orders => {
                val result = orders.map(order => {
                    RentalOrder(
                        order.date,
                        order.videos.filter(video => video.isRental).map(video => {
                            RentalOrderVideo(
                                video.title, video.isReturned)
                        }),
                        order.rentalsDueDate.get,
                        order.isShipped)
                })
                Ok(Json.toJson(result))
            }).recover(ErrorHandler("Could not fetch rental orders for customer"))
        }

    case class RentalOrder(
        date: Date,
        videos: List[RentalOrderVideo],
        dueDate: Date,
        isShipped: Boolean
    )

    case class RentalOrderVideo(
        title: String,
        isReturned: Boolean
    )

    /**
     * Creates an order in the database from the given order object.
     */
    def createOrder(): Action[JsValue] = JwtSigned.async(parse.json) { request =>
        val result = request.body.validate[Order]
        result match {
            case JsSuccess(order, _) => orderRepository.addOrder(order)
                .flatMap(_ => onOrderCreated(order)).map(_ => Ok(order._id.toHexString))
                .recover(ErrorHandler("Could not add order"))
            case JsError(errors) => JsErrorHandler("Order validation failed",
                JsError(errors))
        }
    }

    /**
     * Is called when the given order has been created in the database.
     */
    private def onOrderCreated(order: Order): Future[Unit] = {
        // for each video in the order, change its status to rented/sold
        // to the customer who placed the order
        order.videos.foreach(
            video => updateVideoToReflectOrder(video, order.customer._id.toHexString))

        // if any credit was applied for the order
        if (order.creditApplied > 0) {
            // decrease the customer's credit by the amount which was applied
            val debitAmount = Math
                .min(order.creditApplied, order.customer.credit.getOrElse(0.toFloat))
            customerRepository
                .addToCustomerCredit(order.customer._id.toHexString, -debitAmount)
        }

        sendEmailToCustomer(order).map(_ => sendEmailToAdmins(order))
    }

    /**
     * Updates the given video in the database to reflect that it has been rented or sold
     * by the customer of the given ID.
     */
    private def updateVideoToReflectOrder(video: OrderVideo, customerId: String): Unit = {
        val fetch = videoRepository.fetchVideo(video._id.toHexString)
        fetch.recover(ErrorHandler("Could not fetch video which was ordered"))
        fetch.map(_.foreach(updateVideoToReflectOrder(_, customerId)))
            .recover(ErrorHandler("Could not update video which was ordered"))
    }

    private def updateVideoToReflectOrder(video: Video, customerId: String): Unit = {
        // update the video's status to reflect that it has been rented or sold
        video.status = if (video.isRental) VideoStatuses.rented.id else VideoStatuses.sold
            .id
        video.statusCustomerId = Some(new ObjectId(customerId))
        video.statusDate = Some(new Date())
        checkForConsignmentCredit(video)
        videoRepository.updateVideo(video)
    }

    /**
     * Checks whether the given video is on consignment for a customer, and if it is,
     * credits that customer for it being sold.
     */
    private def checkForConsignmentCredit(video: Video): Unit = {
        // if the video is a rental, no credit may be given
        if (video.isRental) return

        // if the video has no associated consignment customer,
        // no credit may be given
        if (video.consignmentCustomerId.isEmpty) return

        // credit the consignment customer for the given video being sold
        val credit = 5f
        customerRepository
            .addToCustomerCredit(video.consignmentCustomerId.get.toHexString, credit)
            .recover(ErrorHandler("Could not add credit to consignment customer"))

        // store the credit amount in the video itself, so there is a record
        // of it to draw from for when the customer views their consignment
        // videos table
        video.creditGiven = Some(credit)
    }

    private def addText(s: Option[String]) = s.getOrElse("")

    private def addTextLine(s: Option[String], label: String = "") = if (s.isDefined)
        "\n" + label + s.get else ""

    /**
     * Sends a notification e-mail to all should-notify admins, which describes
     * the given order.
     */
    private def sendEmailToAdmins(order: Order): Future[Unit] = {
        // form the e-mail text from the given order
        val customer = order.customer
        val text = "Dear Discount Video admin:" +
            "\n\nThis message is to inform you of a recent order to Discount Video." +
            "\n\nOrder details" + "\n-------------" +
            composeOrderText(order, isToCustomer = false) + "\n\nCustomer details" +
            "\n----------------" + "\n" + customer.firstName + " " + customer.lastName +
            addTextLine(customer.address) + "\n" + addText(customer.city) + ", " +
            addText(customer.state) + " " + addText(customer.zipCode) +
            addTextLine(customer.phoneNumber, "Phone: ") +
            addTextLine(customer.emailAddress, "E-mail: ")

        // when we find all admins in the database which should be notified of orders
        val future = adminRepository.fetchAdminsWhoShouldBeNotifiedOfOrders().map(admins => {
            admins.foreach(admin => Email.send(admin.emailAddress,
                "Notification of order to Discount Video for " +
                    Util.asMoney(order.finalPrice, shouldJustify = false), text))
        })

        if (shouldPrint) Util.printText(text)

        future
    }

    /**
     * Sends an order confirmation e-mail to the customer who made the given order.
     */
    private def sendEmailToCustomer(order: Order): Future[Unit] = {
        val text = "This message is to confirm your recent order to Discount Video." +
            "\n\nYour order" + "\n----------" +
            composeOrderText(order, isToCustomer = true) +
            "\n\nThank you for your business." + "\n\nSincerely,\nDiscount Video" +
            "\nhttps://gaydiscountvideo.com"
        Email.send(order.customer.emailAddress.get,
            "Order confirmation from Discount Video", text)
    }

    /**
     * Returns a textual description of the given order, suitable for inclusion in
     * an notification e-mail to a customer or admin.
     */
    private def composeOrderText(order: Order, isToCustomer: Boolean): String = {
        val dateTimeFormat = DateFormat
            .getDateTimeInstance(DateFormat.LONG, DateFormat.LONG)
        var text = "\nDate: " + dateTimeFormat.format(new Date()) + "\n\nItems ordered:\n"
        val asMoney = Util.asMoney(_: Float)
        order.videos.foreach(video => {
            text +=
                "\n    " + (if (video.isRental) "Rental of" else "Purchase of") + " " +
                    video.title +
                    (if (!isToCustomer && video.isRental) " [" + video.code +
                        "]" else "") + ": " +
                    Util.asMoney(video.price, shouldJustify = false)
        })
        text += "\n\n    Subtotal:" + asMoney(order.productsTotal) +
            (if (order.creditApplied > 0) "\n    Credit: " +
                asMoney(-order.creditApplied) else "") + "\n    Tax:     " +
            asMoney(order.tax) + "\n    Shipping:" + asMoney(order.shippingTotal) +
            "\n    Total:   " + asMoney(order.finalPrice)

        if (order.rentalsDueDate.isDefined) {
            val dateFormat = DateFormat.getDateInstance(DateFormat.LONG)
            val beginning = if (isToCustomer) "Please return your rental videos by " else "Rental videos are due back on "
            text += "\n\n" + beginning + dateFormat.format(order.rentalsDueDate.get)
        }

        text
    }

    /**
     * Updates the order of the given ID in the database with the given is-shipped value.
     */
    def updateIsShipped(orderId: String,
        isShipped: Boolean): Action[AnyContent] = JwtSigned.async {
        orderRepository.updateIsShipped(orderId, isShipped)
            .map(_ => Ok("Is-shipped value updated"))
            .recover(ErrorHandler("Could not update is-shipped value"))
    }

    /**
     * Updates the video entry of the given ID, in the order of the given ID in the database,
     * with the given is-returned value. This also modifies the availability status of the
     * corresponding video entity in the database.  In the case of a false such value,
     * therefore, this first checks that the video hasn't already been rented as part
     * of a later order.  If it has, the update is rejected.
     */
    def updateIsReturned(orderId: String, videoId: String,
        isReturned: Boolean): Action[AnyContent] = JwtSigned.async {
        val onCanUpdate = () => onCanUpdateVideoReturnedStatus(orderId, videoId,
            isReturned).map(_ => Ok("Is-returned value of video updated"))
            .recover(ErrorHandler("Could not update is-returned value of video"))

        // if the given is-returned value is false, we first have to check that the video
        // in question has not been moved out of the available state during the time since
        // it was marked as returned
        if (!isReturned) videoRepository.isVideoAvailable(videoId).flatMap(
            result => if (result) onCanUpdate() else Future(
                BadRequest("Video not available for undoing return")))

        // but if the given is-returned value is true, no such check is necessary, as the
        // video is unavailable for selection until it is returned
        else onCanUpdate()
    }

    /**
     * Updates the video entry of the given ID, in the order of the given ID in the database,
     * with the given is-returned value. This also modifies the availability status of the
     * corresponding video entity in the database.
     */
    private def onCanUpdateVideoReturnedStatus(orderId: String, videoId: String,
        isReturned: Boolean): Future[Unit] = {
        updateVideoReturnedStatus(orderId, videoId, isReturned)
            .map(_ => orderRepository.updateIsReturned(orderId, videoId, isReturned))
    }

    /**
     * Updates the status values of the "real" video document to reflect that the
     * video has been returned (or, that its return has been undone).
     */
    private def updateVideoReturnedStatus(orderId: String, videoId: String,
        isReturned: Boolean): Future[Unit] = {
        // update the status values of the "real" video document to reflect that the
        // video has been returned (or, that its return has been undone)
        orderRepository.fetchOrder(orderId).flatMap(orderOpt => {
            if (orderOpt.isEmpty) throw new Exception("Could not find order")
            val order = orderOpt.get

            videoRepository.fetchVideo(videoId).map(videoOpt => {
                if (videoOpt.isEmpty) throw new Exception("Could not find video")

                val video = videoOpt.get
                video.status = if (isReturned) VideoStatuses.available
                    .id else VideoStatuses.rented.id
                video.statusCustomerId = if (isReturned) None else Some(
                    order.customer._id)
                video.statusDate = if (isReturned) None else Some(order.date)
                videoRepository.updateVideo(video)
            })
        })
    }
}

