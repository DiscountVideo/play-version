package videos

import java.util.Date

import app.Repositories
import storage.videos.VideoStatuses

import scala.concurrent.ExecutionContext.Implicits.global

object CartChecker
{
    private val videoRepository = Repositories.createVideoRepository()

    /**
     * Check all carts for videos left behind, which are removed
     * from the carts and made available.
     */
    def removeVideosLeftInCarts(): Unit =
    {
        // when we've found all videos in the database which have been in a cart
        // an hour or longer
        val now = new Date()
        val minutes = 60
        val minutesInMs = minutes * 60 * 1000
        val cutoff = new Date(now.getTime - minutesInMs)
        videoRepository
            .fetchVideosLeftInCarts(cutoff)
            .map(
                videos => {
                    // remove the videos from the carts
                    videos.foreach(
                        video => {
                            video.status = VideoStatuses.available.id
                            video.statusCustomerId = None
                            video.statusDate = None
                            videoRepository.updateVideo(video)
                        })
                }
            )
    }
}
