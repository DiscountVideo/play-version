package videos

import java.util.Date
import app._
import auth.JwtSigned
import org.mongodb.scala.bson.ObjectId
import play.api.libs.json._
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import storage.videos.Video
import storage.{JsonDate, ObjectIdFormat}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.implicitConversions

class VideosController(components: ControllerComponents) extends AbstractController(components) {
    private val videoRepository = Repositories.createVideoRepository()

    private implicit val dateWrites: Writes[Date] = JsonDate.dateWrites
    private implicit val objectIdReads: Reads[ObjectId] = ObjectIdFormat.reads
    private implicit val objectIdWrites: Writes[ObjectId] = ObjectIdFormat.writes
    private implicit val videoFormat: OFormat[Video] = Json.format[Video]

    def fetchVideo(videoId: String): Action[AnyContent] = Action.async {
        videoRepository.fetchVideo(videoId).map(video => Ok(Json.toJson(video)))
            .recover(ErrorHandler("Could not fetch video"))
    }

    def fetchVideos(isForRentals: Boolean, pageNumber: Int, videosPerPage: Int,
        firstLetter: Option[String], titleSearchText: Option[String],
        searchTag: Option[String], searchStudio: Option[String],
        codeSearchText: Option[String]): Action[AnyContent] = Action.async {
        videoRepository.fetchVideos(isForRentals, pageNumber, videosPerPage, firstLetter,
            titleSearchText, searchTag, searchStudio, codeSearchText).map(
            fetch => Ok(Json.obj("videos" -> Json.toJson(fetch._1), "count" -> fetch._2)))
            .recover(ErrorHandler("Could not fetch videos"))
    }

    def fetchAllTags(): Action[AnyContent] = Action.async {
        videoRepository.fetchAllTags().map(tags => Ok(Json.toJson(tags)))
            .recover(ErrorHandler("Could not fetch all tags"))
    }

    def fetchTags(isForRentals: Boolean): Action[AnyContent] = Action.async {
        videoRepository.fetchTags(isForRentals).map(tags => Ok(Json.toJson(tags)))
            .recover(ErrorHandler("Could not fetch tags"))
    }

    def fetchAllStudios(): Action[AnyContent] = Action.async {
        videoRepository.fetchAllStudios().map(studios => Ok(Json.toJson(studios)))
            .recover(ErrorHandler("Could not fetch all studios"))
    }

    def fetchStudios(isForRentals: Boolean): Action[AnyContent] = Action.async {
        videoRepository.fetchStudios(isForRentals)
            .map(studios => Ok(Json.toJson(studios)))
            .recover(ErrorHandler("Could not fetch studios"))
    }

    def fetchCartVideos(customerId: String): Action[AnyContent] = JwtSigned.async {
        videoRepository.fetchCartVideos(customerId).map(videos => Ok(
            Json.obj("rentalVideos" -> Json.toJson(videos._1),
                "forSaleVideos" -> Json.toJson(videos._2))))
            .recover(ErrorHandler("Could not fetch cart videos"))
    }

    def addCartVideo(videoId: String, customerId: String): Action[AnyContent] = JwtSigned
        .async {
            videoRepository.addCartVideo(videoId, customerId).map(_ => NoContent)
                .recover(ErrorHandler("Could not add video to cart"))
        }

    def removeCartVideo(videoId: String): Action[AnyContent] = JwtSigned.async {
        videoRepository.removeCartVideo(videoId).map(_ => NoContent)
            .recover(ErrorHandler("Could not remove video from cart"))
    }

    def fetchConsignmentVideos(customerId: String): Action[AnyContent] = JwtSigned.async {
        videoRepository.fetchConsignmentVideos(customerId)
            .map(videos => Ok(Json.toJson(videos)))
            .recover(ErrorHandler("Could not fetch consignment videos"))
    }

    def addVideo(): Action[JsValue] = JwtSigned.async(parse.json) { request =>
        request.body.validate[Video] match {
            case JsSuccess(video, _) => videoRepository.addVideo(video).map(_ => Ok)
                .recover(ErrorHandler("Could not add video"))
            case JsError(errors) => Future(BadRequest(JsError.toJson(errors)))
        }
    }

    def updateVideo(): Action[JsValue] = JwtSigned.async(parse.json) { request =>
        request.body.validate[Video] match {
            case JsSuccess(video, _) => videoRepository.updateVideo(video).map(_ => Ok)
                .recover(ErrorHandler("Could not update video"))
            case JsError(errors) => Future(BadRequest(JsError.toJson(errors)))
        }
    }

    def deleteVideo(videoId: String): Action[AnyContent] = JwtSigned.async {
        videoRepository.deleteVideo(videoId).map(_ => NoContent)
            .recover(ErrorHandler("Could not delete video"))
    }
}
