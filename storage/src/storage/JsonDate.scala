package storage

import java.text.SimpleDateFormat
import java.util.Date

import play.api.libs.json._

object JsonDate
{
    val jsDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")

    def format(ms: Long) = jsDateFormat.format(ms)

    def parse(s: String): Date = jsDateFormat.parse(s)

    val dateReads =
        new Reads[Date] {
            def reads(jv: JsValue) =
                jv match {
                    case s: JsString => JsSuccess(JsonDate.parse(s.value))
                    case _ => JsError("Order date is not a string")
                }
        }

    val dateWrites =
        new Writes[Date] {
            def writes(d: Date): JsValue = Json.toJson(jsDateFormat.format(d))
        }
}
