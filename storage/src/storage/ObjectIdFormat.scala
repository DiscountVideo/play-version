package storage

import org.mongodb.scala.bson.ObjectId
import play.api.libs.json._

object ObjectIdFormat {
    val reads: Reads[ObjectId] = new Reads[ObjectId] {
        def reads(value: JsValue): JsResult[ObjectId] = if (value.asOpt[String]
            .isDefined) JsSuccess(new ObjectId(value.as[String])) else JsSuccess(
            new ObjectId())
    }

    val writes: Writes[ObjectId] = new Writes[ObjectId] {
        def writes(value: ObjectId): JsValue = Json.toJson(value.toHexString)
    }
}
