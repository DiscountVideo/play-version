package storage.admins

import org.mongodb.scala.bson.ObjectId

case class Admin(
    var _id: ObjectId,
    emailAddress: String,
    var hash: String,
    var salt: String,
    shouldNotifyOfOrders: Boolean
)
