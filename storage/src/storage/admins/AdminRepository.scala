package storage.admins

import scala.concurrent.Future

trait AdminRepository
{
    def fetchAdminByEmailAddress(emailAddress: String): Future[Option[Admin]]
    def fetchAdmin(adminId: String): Future[Option[Admin]]
    def fetchAdmins(): Future[Seq[Admin]]
    def fetchAdminsWhoShouldBeNotifiedOfOrders(): Future[Seq[Admin]]
    def addAdmin(admin: Admin): Future[Unit]
    def updateShouldNotify(adminId: String, shouldNotify: Boolean): Future[Unit]
    def deleteAdmin(adminId: String): Future[Unit]
    def updateAdminHash(id: String, hash: String, salt: String): Future[Unit]
}
