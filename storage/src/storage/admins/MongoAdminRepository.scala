package storage.admins

import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Updates._

import scala.concurrent.Future

class MongoAdminRepository(database: MongoDatabase) extends AdminRepository {
    val codecRegistry: CodecRegistry = fromRegistries(fromProviders(classOf[Admin]),
        DEFAULT_CODEC_REGISTRY)

    private lazy val collection = database.getCollection[Admin]("Admins")
        .withCodecRegistry(codecRegistry)

    def fetchAdminByEmailAddress(emailAddress: String): Future[Option[Admin]] = {
        val filter = equal("emailAddress", emailAddress.toLowerCase())
        collection.find(filter).first().headOption()
    }

    def fetchAdmin(adminId: String): Future[Option[Admin]] = collection
        .find(equal("_id", new ObjectId(adminId))).headOption()

    def fetchAdmins(): Future[Seq[Admin]] = collection.find()
        .sort(ascending("emailAddress")).collect().head()

    def fetchAdminsWhoShouldBeNotifiedOfOrders(): Future[Seq[Admin]] = collection
        .find(equal("shouldNotifyOfOrders", true)).collect().head()

    def addAdmin(admin: Admin): Future[Unit] = collection.insertOne(admin).map(_ => {})
        .head()

    def updateShouldNotify(adminId: String,
                           shouldNotify: Boolean): Future[Unit] = collection
        .updateOne(equal("_id", new ObjectId(adminId)),
            set("shouldNotifyOfOrders", shouldNotify)).map(_ => {}).head()

    def deleteAdmin(adminId: String): Future[Unit] = collection
        .deleteOne(equal("_id", new ObjectId(adminId))).map(_ => {}).head()

    def updateAdminHash(adminId: String, hash: String, salt: String): Future[Unit] = {
        val update = combine(set("hash", hash), set("salt", salt))
        collection.updateOne(equal("_id", new ObjectId(adminId)), update).map(_ => {})
            .head()
    }
}
