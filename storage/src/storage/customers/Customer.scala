package storage.customers

import org.mongodb.scala.bson.ObjectId

case class Customer(
    var _id: ObjectId,
    firstName: String,
    lastName: String,
    address: Option[String] = None,
    city: Option[String] = None,
    state: Option[String] = None,
    zipCode: Option[String] = None,
    phoneNumber: Option[String] = None,
    var emailAddress: Option[String] = None,
    credit: Option[Float] = None
)

case class CustomerPasswordHash(
    // should equal that of the customer whose password-hash this is
    var _id: ObjectId,
    var hash: Option[String] = None,
    var salt: Option[String] = None
)
