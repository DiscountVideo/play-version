package storage.customers

import scala.concurrent.Future

trait CustomerRepository
{
    def fetchCustomer(id: String): Future[Option[Customer]]
    def fetchCustomers(
        pageNumber: Int, customersPerPage: Int, firstLetter: Option[String],
        lastNameSearchText: Option[String]): Future[(Seq[Customer], Long)]
    def updateCustomer(customer: Customer): Future[Unit]
    def addToCustomerCredit(id: String, amount: Float): Future[Unit]
    def fetchCustomerByEmailAddress(emailAddress: String): Future[Option[Customer]]
    def fetchPasswordHash(id: String): Future[Option[CustomerPasswordHash]]
    def addCustomer(customer: Customer): Future[Unit]
    def addPasswordHash(password: CustomerPasswordHash): Future[Unit]
    def updatePasswordHash(id: String, hash: String, salt: String): Future[Unit]
    def fetchNames(): Future[Seq[Customer]]
}
