package storage.customers

import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters.{regex, _}
import org.mongodb.scala.model.Projections._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Updates._

import scala.concurrent.Future

class MongoCustomerRepository(database: MongoDatabase) extends CustomerRepository {
    val codecRegistry: CodecRegistry = fromRegistries(
        fromProviders(classOf[Customer], classOf[CustomerPasswordHash]),
        DEFAULT_CODEC_REGISTRY)

    private lazy val customerCollection = database.getCollection[Customer]("Customers")
        .withCodecRegistry(codecRegistry)
    private lazy val passwordHashCollection = database
        .getCollection[CustomerPasswordHash]("CustomerPasswordHashes")
        .withCodecRegistry(codecRegistry)

    def fetchCustomer(id: String): Future[Option[Customer]] = customerCollection
        .find(equal("_id", new ObjectId(id))).first().headOption()

    def fetchPasswordHash(
        id: String): Future[Option[CustomerPasswordHash]] = passwordHashCollection
        .find(equal("_id", new ObjectId(id))).first().headOption()

    def fetchCustomers(pageNumber: Int, customersPerPage: Int,
        firstLetter: Option[String],
        lastNameSearchText: Option[String]): Future[(Seq[Customer], Long)] = {
        val numToSkip = pageNumber * customersPerPage

        // since it is possible that no filters will be requested, start with a
        // nominal filter, so we don't wind up with no filter, below
        var query = notEqual("_id", null)

        if (lastNameSearchText.isDefined) {
            query = and(query, regex("lastName", lastNameSearchText.get, "i"))
        } else if (firstLetter.isDefined) {
            val pattern = s"(^${firstLetter.get})(.*)"
            query = and(query, regex("lastName", pattern, "i"))
        }

        val find = customerCollection.find(query).skip(numToSkip).limit(customersPerPage)
            .sort(ascending("lastName")).collect()
        find.flatMap(
            customers => customerCollection.countDocuments(query).map((customers, _)))
            .head()
    }

    def updateCustomer(customer: Customer): Future[Unit] = {
        if (customer.emailAddress.isDefined) customer.emailAddress = Some(
            customer.emailAddress.get.toLowerCase)
        customerCollection.replaceOne(equal("_id", customer._id), customer).map(_ => {})
            .head()
    }

    def addToCustomerCredit(id: String, amount: Float): Future[Unit] = {
        customerCollection
            .updateOne(equal("_id", new ObjectId(id)), inc("credit", amount)).map(_ => {})
            .head()
    }

    def fetchCustomerByEmailAddress(emailAddress: String): Future[Option[Customer]] = {
        val filter = equal("emailAddress", emailAddress.toLowerCase())
        customerCollection.find(filter).first().headOption()
    }

    def addCustomer(customer: Customer): Future[Unit] = customerCollection
        .insertOne(customer).map(_ => {}).head()

    def addPasswordHash(password: CustomerPasswordHash): Future[Unit] = {
        passwordHashCollection.insertOne(password).map(_ => {}).head()
    }

    def updatePasswordHash(id: String, hash: String, salt: String): Future[Unit] = {
        val filter = equal("_id", new ObjectId(id))
        val update = combine(set("hash", hash), set("salt", salt))
        passwordHashCollection.updateOne(filter, update).map(_ => {}).head()
    }

    def fetchNames(): Future[Seq[Customer]] = {
        customerCollection.find().projection(include("lastName", "firstName"))
            .sort(ascending("lastName")).collect().head()
    }
}
