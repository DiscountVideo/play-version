package storage.mongo

import org.mongodb.scala._

object MongoRepository {
    private lazy val productionClient = MongoClient("mongodb://localhost:27017")
    private lazy val testClient = MongoClient("mongodb://localhost:27000")

    lazy val productionDatabase: MongoDatabase = productionClient
        .getDatabase("DiscountVideo")
    lazy val testDatabase: MongoDatabase = testClient.getDatabase("DiscountVideoTest")
}
