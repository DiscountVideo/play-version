package storage.orders

import java.util.Date

import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters.{regex, _}
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Updates._
import storage.customers.Customer

import scala.concurrent.Future

class MongoOrderRepository(database: MongoDatabase) extends OrderRepository {
    val codecRegistry: CodecRegistry = fromRegistries(
        fromProviders(classOf[Order], classOf[Customer], classOf[OrderVideo]),
        DEFAULT_CODEC_REGISTRY)

    private lazy val collection = database.getCollection[Order]("Orders")
        .withCodecRegistry(codecRegistry)

    def fetchOrder(id: String): Future[Option[Order]] = collection
        .find(equal("_id", new ObjectId(id))).headOption()

    def fetchOrders(pageNumber: Int, ordersPerPage: Int, firstLetter: Option[String],
        customerNameSearchText: Option[String], shouldExcludeShipped: Boolean,
        shouldLimitToOverdue: Boolean,
        shouldExcludeReturned: Boolean): Future[(Seq[Order], Long)] = {
        val numToSkip = pageNumber * ordersPerPage

        // since it is possible that no filters will be requested, start with a
        // nominal filter, so we don't wind up with no filter, below
        var query = notEqual("_id", null)

        if (customerNameSearchText.isDefined) {
            query = and(query, regex("customer.lastName", customerNameSearchText.get, "i"))
        } else if (firstLetter.isDefined) {
            val pattern = s"(^${firstLetter.get})(.*)"
            query = and(query, regex("customer.lastName", pattern, "i"))
        }

        // determine whether only un-shipped videos should be included
        if (shouldExcludeShipped) query = and(query, equal("isShipped", false))

        // determine whether only overdue videos should be included
        if (shouldLimitToOverdue) {
            query = and(query, equal("videos.isRental", true), equal("isShipped", true),
                equal("videos.isReturned", false), lt("rentalsDueDate", new Date()))
        }

        // determine whether only orders with rental videos which haven't been returned
        // should be included
        if (shouldExcludeReturned) {
            query = and(query, equal("videos.isRental", true), equal("isShipped", true),
                equal("videos.isReturned", false))
        }

        val find = collection.find(query).skip(numToSkip).limit(ordersPerPage)
            .sort(orderBy(descending("date"))).collect()
        find.flatMap(orders => collection.countDocuments(query).map((orders, _))).head()
    }

    def fetchRentalOrdersForCustomer(customerId: String): Future[Seq[Order]] = {
        var query = equal("customer._id", new ObjectId(customerId))
        query = and(query, equal("videos.isRental", true))
        collection.find(query)
            .sort(orderBy(ascending("dueDate"))).collect().head()
    }

    def addOrder(order: Order): Future[Unit] = collection.insertOne(order).map(_ => {})
        .head()

    def updateIsShipped(orderId: String, isShipped: Boolean): Future[Unit] = {
        collection
            .updateOne(equal("_id", new ObjectId(orderId)), set("isShipped", isShipped))
            .map(_ => {}).head()
    }

    def updateIsReturned(orderId: String, videoId: String,
        isReturned: Boolean): Future[Unit] = {
        val filter = and(equal("_id", new ObjectId(orderId)),
            equal("videos._id", new ObjectId(videoId)))
        collection.updateOne(filter, set("videos.$.isReturned", isReturned)).map(_ => {})
            .head()
    }
}
