package storage.orders

import java.util.Date

import org.mongodb.scala.bson.ObjectId
import storage.customers.Customer

case class Order(
    var _id: ObjectId,
    date: Date,
    customer: Customer,
    videos: List[OrderVideo],
    rentalsDueDate: Option[Date],
    productsTotal: Float,
    creditApplied: Float,
    tax: Float,
    shippingTotal: Float,
    finalPrice: Float,
    isShipped: Boolean
)

case class OrderVideo(
    var _id: ObjectId,
    code: Option[String],
    title: String,
    isRental: Boolean,
    price: Float,
    isReturned: Boolean
)