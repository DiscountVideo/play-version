package storage.orders

import scala.concurrent.Future

trait OrderRepository
{
    def fetchOrder(id: String): Future[Option[Order]]
    def fetchOrders(
        pageNumber: Int, ordersPerPage: Int, firstLetter: Option[String],
        customerNameSearchText: Option[String],
        shouldExcludeShipped: Boolean, shouldLimitToOverdue: Boolean,
        shouldExcludeReturned: Boolean): Future[(Seq[Order], Long)]
    def fetchRentalOrdersForCustomer(customerId: String): Future[Seq[Order]]
    def addOrder(order: Order): Future[Unit]
    def updateIsShipped(orderId: String, isShipped: Boolean): Future[Unit]
    def updateIsReturned(orderId: String, videoId: String, isReturned: Boolean):
        Future[Unit]
}
