package storage.other

import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.{MongoClient, MongoDatabase}
import storage.videos.Video

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.util.Random

object ConvertVideosToRentals {
    val codecRegistry: CodecRegistry = fromRegistries(fromProviders(classOf[Video]),
        DEFAULT_CODEC_REGISTRY)

    val database: MongoDatabase = MongoClient("mongodb://localhost:27017")
        .getDatabase("DiscountVideo")

    private lazy val collection = database.getCollection[Video]("Videos")
        .withCodecRegistry(codecRegistry)

    def main(args: Array[String]): Unit = {
        val numVideosToConvert = 100
        val videosFuture = collection.find().collect().head().map(videos => {
            val ids = (0 to numVideosToConvert).map(_ =>
                videos(Random.nextInt(videos.size))._id
            ).toList
            val updateFuture = collection
                .updateMany(in("_id", ids: _*), set("isRental", true)).head()
            println(Await.result(updateFuture, Duration.Inf))
        })
        Await.result(videosFuture, Duration.Inf)
    }
}
