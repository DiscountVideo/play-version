package storage.videos

import java.util.Date

import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Projections._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Updates._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class MongoVideoRepository(database: MongoDatabase) extends VideoRepository {
    val codecRegistry: CodecRegistry = fromRegistries(fromProviders(classOf[Video]),
        DEFAULT_CODEC_REGISTRY)

    private lazy val collection = database.getCollection[Video]("Videos")
        .withCodecRegistry(codecRegistry)

    def fetchVideo(id: String): Future[Option[Video]] = collection
        .find(equal("_id", new ObjectId(id))).headOption()

    def fetchVideos(isForRentals: Boolean, pageNumber: Int, videosPerPage: Int,
        firstLetter: Option[String], titleSearchText: Option[String],
        searchTag: Option[String], searchStudio: Option[String],
        codeSearchText: Option[String]): Future[(Seq[Video], Long)] = {
        val numToSkip = pageNumber * videosPerPage

        var query = equal("isRental", isForRentals)

        if (titleSearchText.isDefined) {
            query = and(query, regex("title", titleSearchText.get, "i"))
        } else if (firstLetter.isDefined) {
            val subPattern = if (firstLetter.get == "0") """\d""" else firstLetter.get
            val pattern = s"(^$subPattern)(.*)"
            query = and(query, regex("title", pattern, "i"))
        }

        if (searchTag.isDefined) query = and(query, all("tags", searchTag.get))
        if (searchStudio.isDefined) query = and(query, equal("studio", searchStudio.get))

        if (codeSearchText.isDefined) {
            query = and(query, regex("code", codeSearchText.get))
        }

        // if we are to find for-sale videos, exclude those which have been sold
        if (!isForRentals) query = and(query, notEqual("status", VideoStatuses.sold.id))

        val find = collection.find(query).skip(numToSkip).limit(videosPerPage)
            .sort(ascending("title")).collect()
        find.flatMap(videos => collection.countDocuments(query).map((videos, _))).head()
    }

    def fetchAllTags(): Future[Seq[String]] = {
        collection.distinct[String]("tags").collect().map(_.sorted).head()
    }

    def fetchTags(isForRentals: Boolean): Future[Seq[String]] = {
        collection.distinct[String]("tags", equal("isRental", isForRentals)).collect()
            .map(_.sorted).head()
    }

    def fetchAllStudios(): Future[Seq[String]] = {
        collection.distinct[String]("studio").collect().map(_.sorted).head()
    }

    def fetchStudios(isForRentals: Boolean): Future[Seq[String]] = {
        collection.distinct[String]("studio", equal("isRental", isForRentals)).collect()
            .map(_.sorted).head()
    }

    def fetchCartVideos(customerId: String): Future[(Seq[Video], Seq[Video])] = {
        val cartVideos = collection.find(and(equal("status", VideoStatuses.inCart.id),
            equal("statusCustomerId", new ObjectId(customerId)))).sort(ascending("title"))
            .collect()
        cartVideos.map(videos => {
            val rentalVideos = videos.filter(_.isRental)
            val forSaleVideos = videos.filter(!_.isRental)
            (rentalVideos, forSaleVideos)
        }).head()
    }

    def addCartVideo(videoId: String, customerId: String): Future[Unit] = {
        fetchVideo(videoId).flatMap(videoOpt => {
            if (videoOpt.isEmpty) throw new Exception("Video not found")

            // if the video is not available for putting into a cart, throw an error
            if (videoOpt.get.status != VideoStatuses.available.id) throw new Exception(
                "Video not available to put in cart")

            // set the video as being in the customer's cart
            val update = combine(set("status", VideoStatuses.inCart.id),
                set("statusCustomerId", new ObjectId(customerId)),
                set("statusDate", new Date()))
            collection.updateOne(equal("_id", new ObjectId(videoId)), update).map(_ => {})
                .head()
        })
    }

    def removeCartVideo(videoId: String): Future[Unit] = {
        val update = combine(set("status", VideoStatuses.available.id),
            unset("statusCustomerId"), unset("statusDate"))
        collection.updateOne(equal("_id", new ObjectId(videoId)), update).map(_ => {})
            .head()
    }

    def addVideo(video: Video): Future[Unit] = collection.insertOne(video).map(_ => {})
        .head()

    def updateVideo(video: Video): Future[Unit] = {
        collection.replaceOne(equal("_id", video._id), video).map(_ => {}).head()
    }

    def fetchConsignmentVideos(customerId: String): Future[Seq[Video]] = {
        val query = and(equal("isRental", false),
            equal("consignmentCustomerId", new ObjectId(customerId)))
        val projection = include("title", "isRental", "status", "statusDate",
            "consignmentDate", "creditGiven")
        collection.find(query).projection(projection).sort(ascending("title")).collect()
            .head()
    }

    def deleteVideo(videoId: String): Future[Unit] = {
        collection.deleteOne(equal("_id", new ObjectId(videoId))).map(_ => {}).head()
    }

    def isVideoAvailable(videoId: String): Future[Boolean] = {
        val filter = and(equal("_id", new ObjectId(videoId)),
            equal("status", VideoStatuses.available.id))
        collection.countDocuments(filter).map(_ > 0).head()
    }

    def fetchVideosLeftInCarts(cutoff: Date): Future[Seq[Video]] = {
        val filter = and(equal("status", VideoStatuses.inCart.id),
            lte("statusDate", cutoff))
        collection.find(filter).collect().head()
    }
}
