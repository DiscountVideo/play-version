package storage.videos

import java.util.Date

import org.mongodb.scala.bson.ObjectId

case class Video(
    _id: ObjectId,
    code: Option[String] = None,
    title: Option[String],
    studio: Option[String] = None,
    director: Option[String] = None,
    year: Option[Int] = None,
    tags: Option[List[String]] = None,
    isRental: Boolean = true,
    var status: Int = VideoStatuses.available.id,
    var statusCustomerId: Option[ObjectId] = None,
    var statusDate: Option[Date] = None,
    consignmentCustomerId: Option[ObjectId] = None,
    consignmentDate: Option[Date] = None,
    var creditGiven: Option[Double] = None
)
