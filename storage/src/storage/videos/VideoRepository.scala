package storage.videos

import java.util.Date

import scala.concurrent.Future

trait VideoRepository
{
    def fetchVideo(id: String): Future[Option[Video]]
    def fetchVideos(
        isForRentals: Boolean, pageNumber: Int, videosPerPage: Int,
        firstLetter: Option[String], titleSearchText: Option[String],
        searchTag: Option[String], searchStudio: Option[String],
        codeSearchText: Option[String]): Future[(Seq[Video], Long)]

    def fetchAllTags(): Future[Seq[String]]
    def fetchTags(isForRentals: Boolean): Future[Seq[String]]

    def fetchAllStudios(): Future[Seq[String]]
    def fetchStudios(isForRentals: Boolean): Future[Seq[String]]

    def fetchCartVideos(customerId: String): Future[(Seq[Video], Seq[Video])]
    def addCartVideo(videoId: String, customerId: String): Future[Unit]
    def removeCartVideo(videoId: String): Future[Unit]

    def addVideo(video: Video): Future[Unit]
    def updateVideo(video: Video): Future[Unit]
    def deleteVideo(videoId: String): Future[Unit]

    def fetchConsignmentVideos(customerId: String): Future[Seq[Video]]

    def isVideoAvailable(videoId: String): Future[Boolean]

    def fetchVideosLeftInCarts(cutoff: Date): Future[Seq[Video]]
}
