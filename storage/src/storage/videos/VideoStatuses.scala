package storage.videos

object VideoStatuses
{
    val available = VideoStatus(id = 0, display = "available")
    val inCart = VideoStatus(id = 1, display = "in cart")
    val rented = VideoStatus(id = 2, display = "rented")
    val sold = VideoStatus(id = 3, display = "sold")
}

case class VideoStatus(id: Int, display: String)
